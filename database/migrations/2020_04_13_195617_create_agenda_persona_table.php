<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_persona', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('start'); //fecha y hora de evento
            $table->dateTime('end'); //fecha y hora evento
            $table->string('backgroundColor', 30);
            $table->string('textColor', 30);
            $table->time('endTime');
            $table->time('startTime');

            $table->unsignedBigInteger('persona_id'); //persona id
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->unsignedBigInteger('agenda_id');
            $table->foreign('agenda_id')->references('id')->on('agendas');
            //Mejorar para relizar eliminacion en cascada
            // $table->foreign('agenda_id')->references('id')->on('agendas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_persona');
    }
}
