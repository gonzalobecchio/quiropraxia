<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelvisPatronCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelvis_patron', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pelvis_id');
            $table->unsignedBigInteger('persona_id');
            $table->foreign('pelvis_id')->references('id')->on('pelvis');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelvis_patron_create');
    }
}
