<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controls', function (Blueprint $table) {
            $table->bigIncrements('id');

            //Verificar para nuclearlos en menos campos
            $table->string('x_supino_izq_1')->nullable(); // V o F si esta selelecionado o no
            $table->string('x_supino_izq_2')->nullable();
            $table->string('x_supino_izq_3')->nullable();
            $table->string('x_supino_izq_4')->nullable();
            $table->string('x_supino_der_1')->nullable();
            $table->string('x_supino_der_2')->nullable();
            $table->string('x_supino_der_3')->nullable();
            $table->string('x_supino_der_4')->nullable();
            $table->string('x_prono_izq_1')->nullable();
            $table->string('x_prono_izq_2')->nullable();
            $table->string('x_prono_izq_3')->nullable();
            $table->string('x_prono_izq_4')->nullable();
            $table->string('x_prono_der_1')->nullable();
            $table->string('x_prono_der_2')->nullable();
            $table->string('x_prono_der_3')->nullable();
            $table->string('x_prono_der_4')->nullable();

            $table->string('giro_cab')->nullable();
            $table->string('pelvis')->nullable();
            $table->string('rest_sacra')->nullable();
            $table->string('fosa_izq')->nullable(); //Valor numerico de 1 a 10 con decimales
            $table->string('fosa_der')->nullable(); //Valor numerico de 1 a 10 con decimales

            $table->string('cont_palp')->nullable(); //texto
            $table->mediumText('ObservacionControl')->nullable();
            // $table->string('cont_pp')->nullable(); //pre post
            // $table->string('cont_ajus')->nullable(); //ahora texto luego FK ajuste_id

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controls');
    }
}
