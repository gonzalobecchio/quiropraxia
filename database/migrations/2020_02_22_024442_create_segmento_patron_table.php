<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentoPatronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segmento_patron', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('segmento_id');
            $table->unsignedBigInteger('persona_id');
            $table->foreign('segmento_id')->references('id')->on('segmentos');
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('segmento_patron');
    }
}
