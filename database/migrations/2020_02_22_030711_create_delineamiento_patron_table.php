<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelineamientoPatronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delineamiento_patron', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('delineamiento_id');
            $table->unsignedBigInteger('persona_id');
            $table->foreign('delineamiento_id')->references('id')->on('delineamientos');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delineamiento_patron');
    }
}
