<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtlaPatronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atla_patron', function (Blueprint $table) {
          //la relacion es estre Atlas y Persona
            $table->bigIncrements('id');
            $table->foreign('atla_id')->references('id')->on('atlas');
            // $table->foreign('patron_id')->references('id')->on('patrons');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->unsignedBigInteger('atla_id');
            $table->unsignedBigInteger('persona_id');

            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atla_patron');
    }
}
