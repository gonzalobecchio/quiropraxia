<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelineamientoControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delineamiento_control', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('delineamiento_id');
            $table->foreign('delineamiento_id')->references('id')->on('delineamientos');
            $table->unsignedBigInteger('control_id');
            $table->foreign('control_id')->references('id')->on('controls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delineamiento_control');
    }
}
