<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAxisPatronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('axis_patron', function (Blueprint $table) {
          //Relacion enttre Axis y Personas
            $table->bigIncrements('id');
            $table->unsignedBigInteger('axis_id');
            $table->unsignedBigInteger('persona_id');
            $table->foreign('axis_id')->references('id')->on('axes');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('axis_patron');
    }
}
