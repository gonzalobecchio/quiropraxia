<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatronsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrons', function (Blueprint $table) {
            // $table->bigIncrements('id');
            $table->bigInteger('persona_id')->index();
            // $table->foreign('persona_id')->references('id')->on('personas');


            //Realizar tabla hija
            $table->string('x_supino_izq_1')->nullable(); // V o F si esta selelecionado o no
            $table->string('x_supino_izq_2')->nullable();
            $table->string('x_supino_izq_3')->nullable();
            $table->string('x_supino_izq_4')->nullable();
            $table->string('x_supino_der_1')->nullable();
            $table->string('x_supino_der_2')->nullable();
            $table->string('x_supino_der_3')->nullable();
            $table->string('x_supino_der_4')->nullable();
            $table->string('x_prono_izq_1')->nullable();
            $table->string('x_prono_izq_2')->nullable();
            $table->string('x_prono_izq_3')->nullable();
            $table->string('x_prono_izq_4')->nullable();
            $table->string('x_prono_der_1')->nullable();
            $table->string('x_prono_der_2')->nullable();
            $table->string('x_prono_der_3')->nullable();
            $table->string('x_prono_der_4')->nullable();

            $table->string('giro_cab')->nullable();
            $table->string('pelvis')->nullable();
            $table->string('rest_sacra')->nullable();
            $table->string('fosa_izq')->nullable(); //Valor numerico de 1 a 10 con decimales
            $table->string('fosa_der')->nullable(); //Valor numerico de 1 a 10 con decimales

            $table->string('angular_omop')->nullable();
            $table->string('oblicuo_men')->nullable();
            $table->string('oblicuo_may')->nullable();
            $table->string('ecom')->nullable();
            $table->string('com')->nullable();
            $table->string('recto_post_izq')->nullable();
            $table->string('recto_post_der')->nullable();
            $table->string('inter_trans')->nullable();
            $table->string('espelino_cab')->nullable();
            $table->string('espelino_cue')->nullable();
            $table->string('esc_medio')->nullable();
            $table->string('C3')->nullable();
            $table->string('C4')->nullable();
            $table->string('C5')->nullable();
            $table->string('C6')->nullable();
            $table->string('C7')->nullable();
            $table->string('D1')->nullable();
            $table->string('D2')->nullable();
            $table->string('D3')->nullable();
            $table->string('D4')->nullable();
            $table->string('D5')->nullable();
            $table->string('D6')->nullable();
            $table->string('D7')->nullable();
            $table->string('D8')->nullable();
            $table->string('D9')->nullable();
            $table->string('D10')->nullable();
            $table->string('D11')->nullable();
            $table->string('D12')->nullable();
            $table->string('L1')->nullable();
            $table->string('L2')->nullable();
            $table->string('L3')->nullable();
            $table->string('L4')->nullable();
            $table->string('L5')->nullable();
            $table->string('il_cost')->nullable();
            $table->string('dor_l')->nullable();
            $table->string('ser_p_sup')->nullable();
            $table->string('ser_p_inf')->nullable();
            $table->string('SCMa')->nullable();
            $table->string('SCMe')->nullable();
            $table->string('Lig_si')->nullable();
            $table->mediumText('atlas_patr')->nullable();
            $table->mediumText('axis_patr')->nullable();
            $table->mediumText('inferiores_patr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patrons');
    }
}
