<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pers_nomb',40);
            $table->string('pers_apel',40);
            $table->string('pers_dni',10)->unique()->nullable();
            $table->string('pers_domi',50)->nullable();
            $table->string('pers_tele',10)->nullable();
            $table->string('pers_emai',40)->unique()->nullable();
            $table->string('pers_cuit', 40)->unique()->nullable();
            $table->date('pers_naci')->nullable(); //Dia Nacimiento
            $table->integer('pers_edad')->nullable(); // Consultar si v a o no, es calculable
            $table->integer('pers_cahi')->nullable(); // Cantidad de hijos de persona registrada
            $table->string('pers_ocup',40)->nullable();
            $table->string('pers_esci',40)->nullable();
            // $table->unsignedBigInteger('loca_id'); FK ok
            //Validar luego de tener todo terminado
            // $table->foreign('loca_id')->references('id')->on('localidads') FK
            $table->string('loca_id',40)->nullable(); //Se pasa a texto
            $table->bigInteger('provincia_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
