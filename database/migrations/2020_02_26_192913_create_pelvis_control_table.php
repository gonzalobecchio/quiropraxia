<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelvisControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelvis_control', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pelvis_id');
            $table->foreign('pelvis_id')->references('id')->on('pelvis');
            $table->unsignedBigInteger('control_id');
            //Verificando eliminacion en casacada prueba
            $table->foreign('control_id')->references('id')->on('controls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelvis_control');
    }
}
