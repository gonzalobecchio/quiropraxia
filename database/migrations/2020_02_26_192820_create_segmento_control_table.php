<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentoControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segmento_control', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('segmento_id');
            $table->foreign('segmento_id')->references('id')->on('segmentos');
            $table->unsignedBigInteger('control_id');
            $table->foreign('control_id')->references('id')->on('controls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('segmento_control');
    }
}
