<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigInteger('persona_id')->index();
            $table->string('paci_reco',40)->nullable();
            $table->string('paci_ciru',40)->nullable();
            $table->string('paci_frac',40)->nullable();
            $table->string('paci_medi',40)->nullable();
            $table->mediumText('paci_obs1')->nullable(); //¿Recibio cuidado quiropractico anteriormente? ¿Cuando y con quien?
            $table->mediumText('paci_obs2')->nullable(); //Observaciones Profesional
            $table->mediumText('paci_obs3')->nullable(); //Referencias del paciente
            
            // $table->unsignedBigInteger('pers_id');
            // $table->foreign('pers_id')->references('id')->on('personas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
