<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AtlasTableSeeder::class);
        $this->call(AxisTableSeeder::class);
        $this->call(DelineamientoTableSeeder::class);
        $this->call(PelvisTableSeeder::class);
        $this->call(SegmentosTableSeeder::class);
        // $this->call(ProvinciasTableSeeder::class);
        // $this->call(LocalidadTableSeeder::class);
        // $this->call(PersonasTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
