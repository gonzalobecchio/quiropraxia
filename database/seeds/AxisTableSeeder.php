<?php

use Illuminate\Database\Seeder;
use App\Axis;

class AxisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Axis::create(['name' => 'ESL']);
        Axis::create(['name' => 'ESR']);
        Axis::create(['name' => 'ESR-BR']);
        Axis::create(['name' => 'ESR-SR']);
        Axis::create(['name' => 'ESL-BL']);
        Axis::create(['name' => 'ESL-SL']);
        Axis::create(['name' => 'CP-BR']);
        Axis::create(['name' => 'CP-BL']);
        Axis::create(['name' => 'BP-SR']);
        Axis::create(['name' => 'BP-SL']);
        Axis::create(['name' => 'SP-BR']);
        Axis::create(['name' => 'SP-BL']);

        Axis::create(['name' => 'AXPL']);
        Axis::create(['name' => 'AXPR']);
        Axis::create(['name' => 'AXPLS']);
        Axis::create(['name' => 'AXPLI']);
        Axis::create(['name' => 'AXPRS']);
        Axis::create(['name' => 'AXPRI']);

    }
}
