<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Localidad;
use App\newClass\Utilidades;

class LocalidadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $utilidades = new Utilidades();
      $datosLocalidad = $utilidades->cargarLocalidades();
      for ($i=0; $i < count($datosLocalidad); $i++) {
        Localidad::create(['loca_nomb' => $datosLocalidad[$i]['nombre'],
                           'prov_id' => $datosLocalidad[$i]['provincia_id']]);
      }



        // Localidad::create(['loca_nomb' => 'Santa Fe',
        //                    'prov_id' => DB::table('provincias')->where('prov_nomb', 'Santa Fe')->value('id')]);
        // Localidad::create(['loca_nomb' => 'Santo Tome',
        //                                       'prov_id' => DB::table('provincias')->where('prov_nomb', 'Santa Fe')->value('id')]);
    }
}
