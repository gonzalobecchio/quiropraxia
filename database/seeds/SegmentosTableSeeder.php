<?php

use Illuminate\Database\Seeder;
use App\Segmento;

class SegmentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          Segmento::create(['name'=>'C3']);
          Segmento::create(['name'=>'C4']);
          Segmento::create(['name'=>'C5']);
          Segmento::create(['name'=>'C6']);
          Segmento::create(['name'=>'C7']);
          Segmento::create(['name'=>'D1']);
          Segmento::create(['name'=>'D2']);
          Segmento::create(['name'=>'D3']);
          Segmento::create(['name'=>'D4']);
          Segmento::create(['name'=>'D5']);
          Segmento::create(['name'=>'D6']);
          Segmento::create(['name'=>'D7']);
          Segmento::create(['name'=>'D8']);
          Segmento::create(['name'=>'D9']);
          Segmento::create(['name'=>'D10']);
          Segmento::create(['name'=>'D11']);
          Segmento::create(['name'=>'D12']);
          Segmento::create(['name'=>'L1']);
          Segmento::create(['name'=>'L2']);
          Segmento::create(['name'=>'L3']);
          Segmento::create(['name'=>'L4']);
          Segmento::create(['name'=>'L5']);
    }
}
