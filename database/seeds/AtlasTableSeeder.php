<?php

use Illuminate\Database\Seeder;
use App\Atla;

class AtlasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Atla::create(['name' => 'ASL']);
        Atla::create(['name' => 'ASR']);
        Atla::create(['name' => 'AIL']);
        Atla::create(['name' => 'AIR']);
        Atla::create(['name' => 'ASLA']);
        Atla::create(['name' => 'ASLP']);
        Atla::create(['name' => 'AILA']);
        Atla::create(['name' => 'AILP']);
        Atla::create(['name' => 'ASRA']);
        Atla::create(['name' => 'ASRP']);
        Atla::create(['name' => 'AIRA']);
        Atla::create(['name' => 'AIRP']);

        // Atla::create(['name' => 'AXPL']);
        // Atla::create(['name' => 'AXPR']);
        // Atla::create(['name' => 'AXPLS']);
        // Atla::create(['name' => 'AXPLI']);
        // Atla::create(['name' => 'AXPRS']);
        // Atla::create(['name' => 'AXPRI']);



    }
}
