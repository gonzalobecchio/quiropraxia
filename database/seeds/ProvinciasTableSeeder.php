<?php

use Illuminate\Database\Seeder;
use App\Provincia;
use App\newClass\Utilidades;

class ProvinciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // Provincia::create(['prov_nomb' => 'Cordoba']);
          // Provincia::create(['prov_nomb' => 'Mendoza']);
          // Provincia::create(['prov_nomb' => 'Jujuy']);
          // Provincia::create(['prov_nomb' => 'Salta']);
          // Provincia::create(['prov_nomb' => 'Catamarca']);
          // Provincia::create(['prov_nomb' => 'Chaco']);
          // Provincia::create(['prov_nomb' => 'Formosa']);
          // Provincia::create(['prov_nomb' => 'Santa Fe']);
          // Provincia::create(['prov_nomb' => 'La Rioja']);
          // Provincia::create(['prov_nomb' => 'Misiones']);
          // Provincia::create(['prov_nomb' => 'Tucuman']);
          // Provincia::create(['prov_nomb' => 'San Juan']);
          // Provincia::create(['prov_nomb' => 'San Luis']);
          // Provincia::create(['prov_nomb' => 'Santa Cruz']);
          // Provincia::create(['prov_nomb' => 'Rio Negro']);
          // Provincia::create(['prov_nomb' => 'Tierra del Fuego']);
          // Provincia::create(['prov_nomb' => 'Buenos Aires']);
          // Provincia::create(['prov_nomb' => 'Corrientes']);
          // Provincia::create(['prov_nomb' => 'La Pampa']);
          // Provincia::create(['prov_nomb' => 'Chubut']);
          // Provincia::create(['prov_nomb' => 'Entre Rios']);
          // Provincia::create(['prov_nomb' => 'Santiago del Estero']);
          // Provincia::create(['prov_nomb' => 'Neuquen']);

          $utilidades = new Utilidades();
          $array = $utilidades->cargarProvincias();
          $datosProvincia = $utilidades->cargarProvincias();
          for ($i=0; $i < count($array); $i++) {
            Provincia::create(['prov_nomb' => $array[$i]['nombre'],
                               'id' => $array[$i]['id']]);
          }

    }
}
