<?php

use Illuminate\Database\Seeder;
use App\Pelvis;

class PelvisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelvis::create(['name' => 'S-BP']);
        Pelvis::create(['name' => 'S-PL']);
        Pelvis::create(['name' => 'S-PR']);
        Pelvis::create(['name' => 'S-PLI']);
        Pelvis::create(['name' => 'S-PRI']);
        Pelvis::create(['name' => 'APEX P']);
        Pelvis::create(['name' => 'IDPI']);
        Pelvis::create(['name' => 'IIPI']);
        Pelvis::create(['name' => 'IDAS']);
        Pelvis::create(['name' => 'IIAS']);
        Pelvis::create(['name' => 'IDPI IN']);
        Pelvis::create(['name' => 'IDPI EX']);
        Pelvis::create(['name' => 'IDAS IN']);
        Pelvis::create(['name' => 'IDAS EX']);
        Pelvis::create(['name' => 'IIPI IN']);
        Pelvis::create(['name' => 'IIPI EX']);
        Pelvis::create(['name' => 'IIAS IN']);
        Pelvis::create(['name' => 'IIAS EX']);
    }
}
