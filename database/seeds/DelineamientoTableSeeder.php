<?php

use Illuminate\Database\Seeder;
use App\Delineamiento;

class DelineamientoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Delineamiento::create(['name'=>'PR']);
        Delineamiento::create(['name'=>'PL']);
        Delineamiento::create(['name'=>'PRS']);
        Delineamiento::create(['name'=>'PLS']);
        Delineamiento::create(['name'=>'PRI']);
        Delineamiento::create(['name'=>'PLI']);
    }
}
