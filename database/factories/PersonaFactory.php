<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {
    return [
      'pers_nomb' => $faker->firstName,
      'pers_apel' => $faker->lastName,
      'pers_dni' => $faker->randomNumber,
      'pers_domi' => $faker->streetAddress,
      'pers_tele' => $faker->randomNumber,
      'pers_emai' => $faker->email,
      'pers_cuit' => $faker->randomNumber,
      'pers_naci' => $faker->date,
      'pers_edad' => $faker->numberBetween(0,99),
      'pers_cahi' => $faker->randomDigit,
      'pers_ocup' => $faker->jobTitle,
      'pers_esci' => 'Soltero',
        //
    ];
});
