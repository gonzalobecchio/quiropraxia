@extends('layouts.app')

@section('alerts')
@if ($errors->any())
<div class="alert alert-warning" role="alert">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>

@endif
@endsection

@section('content')

<form action="{{route('personas.update', ['id' => $persona->id])}}" method="POST">
  @csrf
  @method('PUT')
  <div class="container">
    <section>
      <span class="section">@if ($form == 'ver') Datos Paciente @else Editar Paciente  @endif </span>
      @yield('alerts')
      <div class="row">
        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="fullname">Nombre * :</label>
          <input type="text" id="fullname" class="form-control"  maxlength="30" name="nombre" value="{{old('nombre',$persona->pers_nomb)}}" @if ($form == 'ver') readonly @endif>
        </div>

        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="apellido">Apellido * : </label>
          <input type="text" id="apellido" class="form-control" maxlength="30" name="apellido" value="{{old('apellido', $persona->pers_apel)}}"  @if ($form == 'ver') readonly @endif >
        </div>
        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="dni">DNI * :</label>
          <input type="text" id="dni" class="form-control" maxlength="8" name="dni" value="{{old('dni', $persona->pers_dni)}}"  @if ($form == 'ver') readonly @endif >
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="dni">Estado Civil * :</label>
          <select class="form-control" name="esci" value="{{old($persona->pers_esci)}}"  @if ($form == 'ver') disabled @endif >
            <option value='Soltero'> Soltero </option>
            <option value="Casado"> Casado</option>
            <option value="Separado"> Separado</option>
            <option value="Divorciado"> Divorciado</option>
            <option value="Viudo">Viudo</option>

          </select>
        </div>
        <div class="col-md-2 col-sm-2  form-group has-feedback">
          <label for="cuit">Cantidad de hijos :</label>
          <input type="number" min="0" max="99" pattern="^[0-9]{1,}$"  value="{{old('cahi',$persona->pers_cahi)}}" name="cahi" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Cantidad de Hijos"  @if ($form == 'ver') readonly @endif >

        </div>
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="domicilio">Domicilio * :</label>
          <input type="text" id="domicilio" class="form-control" maxlength="30" name="domicilio" value="{{old('domicilio', $persona->pers_domi)}}"  @if ($form == 'ver') readonly @endif >
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="telefono">Telefono * :</label>
          <input type="text" id="telefono" pattern="^[0-9]{1,}$" class="form-control" maxlength="10" name="telefono" value="{{old('telefono', $persona->pers_tele)}}"  @if ($form == 'ver') readonly @endif >
        </div>

        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="email">Email (opcional) ejemplo@email.com :</label>
          <input type="text" id="email" class="form-control" maxlength="60" name="email" value="{{old('email', $persona->pers_emai)}}"  @if ($form == 'ver') readonly @endif >
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">CUIT * :</label>
          <input type="text" id="cuit"  class="form-control" maxlength="11" name="cuit" value="{{old('cuit', $persona->pers_cuit)}}"  @if ($form == 'ver') readonly @endif >
        </div>

        <div class="col-md-6 col-sm-6  form-group">
          <label for="cuit">Fecha de Nacimiento * :</label>
          <input type="date" class="form-control" name="nacimiento" value="{{old('nacimiento',$persona->pers_naci)}}"  @if ($form == 'ver') readonly @endif >


        </div>
      </div>

      <div class="row">


        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">Ocupacion (opcional):</label>
          <input type="text" class="form-control" id="inputSuccess3" maxlength="30" name="ocupacion" value="{{old('ocupacion', $persona->pers_ocup)}}"  @if ($form == 'ver') readonly @endif >

        </div>
      </div>

      <div class="row">



      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">Provincia * :</label>
          <select class="form-control" name="provincia" @if ($form == 'ver') disabled @endif >
            <option value="{{$prov->id}}">{{$prov->prov_nomb}}</option>
            @foreach ($provincias as $provincia)
              <option value="{{$provincia->id}}">{{$provincia->prov_nomb}}</option>
            @endforeach

          </select>

        </div>

        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">Localidad * :</label>
          <input class="form-control" type="text" name="localidad" value="{{$persona->loca_id}}">
          {{-- <select class="form-control" name="localidad" @if ($form == 'ver') disabled @endif >
             <option value="{{$loca->id}}">{{$loca->loca_nomb}}</option>
             @foreach ($localidades as $localidad)
              <option value="{{$localidad->id}}">{{$localidad->loca_nomb}}</option>
            @endforeach
          </select> --}}

        </div>
      </div>

    </section>

    <span class="section"> </span>
    <section>
      <span class="section">Datos a completar por el profesional</span>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="">¿Recibio cuidado quiropractico anteriormente? ¿Cuando y con quien?  (opcional)</label>
          <textarea id="message" class="form-control" name="obs1" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('obs1', $persona->paciente->paci_obs1)}}</textarea>
        </div>
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="">Recomendado por (opcional)</label>
          <textarea id="message" class="form-control" name="recomendado" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('recomendado', $persona->paciente->paci_reco)}}</textarea>
        </div>

      </div>

      <div class="row">
        <div class="col-md-6 form-group has-feedback">
          <label for="">Cirujias (opcional)</label>
          <textarea id="message" class="form-control" name="cirujias" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('cirujias', $persona->paciente->paci_ciru)}}</textarea>
        </div>
        <div class="col-md-6  form-group has-feedback">
          <label for="">Fracturas (opcional)</label>
          <textarea id="message" class="form-control" name="fracturas" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('fracturas',$persona->paciente->paci_frac)}}</textarea>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 form-group has-feedback">
          <label for="">Medicamentos (opcional)</label>
          <textarea id="message" class="form-control" name="medicamentos" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('medicamentos', $persona->paciente->paci_medi)}}</textarea>
        </div>
        <div class="col-md-6 form-group has-feedback">
          <label for="">Observaciones Profesional (opcional)</label>
          <textarea id="message" class="form-control" name="obs2" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('obs2', $persona->paciente->paci_obs2)}}</textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 form-group has-feedback">
          <label for="">Referencias del paciente (opcional)</label>
          <textarea id="message" class="form-control" name="obs3" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"  @if ($form == 'ver') readonly @endif >{{old('obs3', $persona->paciente->paci_obs3)}}</textarea>
        </div>
      </div>
    </section>


  </div>
  <span class="section"> </span>
  <div class="">
    @if ($form == 'ver')
       <a href="{{route('personas.edit',$persona->id)}}" class="btn btn-primary btn-sm">Editar</a>
    @else
        <input class="btn btn-primary btn-sm" type="submit" name="" value="Guardar Cambios">
    @endif

  </div>
</form>
@endsection
