@extends('layouts.app')

@section('content')

<div class="container">
    <section>
        <span class="section">Listado de pacientes</span>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="th-sm">Apellido
                    </th>
                    <th class="th-sm">Nombre
                    </th>
                    <th class="th-sm">DNI
                    </th>
                    <th class="th-sm">CUIT
                    </th>
                    <th class="th-sm">Domicilio
                    </th>
                    <th class="th-sm">Telefono
                    </th>
                    {{-- <th class="th-sm">Editar

                    </th>
                    <th class="th-sm">Imprimir

                    </th> --}}
                    <th class="th-sm">Opciones

                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($personas as $persona)
                <tr>

                    <td>{{$persona->pers_apel}}</td>
                    <td>{{$persona->pers_nomb}}</td>

                    <td>{{$persona->pers_dni}}</td>
                    <td>{{$persona->pers_cuit}}</td>
                    <td>{{$persona->pers_domi}}</td>
                    <td>{{$persona->pers_tele}}</td>
                    {{-- <td>
                      <form action="{{route('personas.edit',$persona->id)}}" method="GET">
                        @csrf
                        <a href="{{route('personas.edit',$persona->id)}}"><i class="fa fa-pencil" title="Editar"></i></a>
                      </form>

                    </td>
                    <td><form class="" action="index.html" method="GET">
                      @csrf
                        <a href="#"><i class="fa fa-eye" title="Ver Paciente"></i></a>
                    </form></td> --}}

                    <td>

                     {{-- <form action="{{route('persona.delete', $persona->id)}}" method="post" >
                      @method('delete')
                      @csrf --}}
                      {{-- {{session(['paciente_id' => $persona->id])}} --}}
                      <a href="{{route('personas.edit', [$persona->id,'ver'])}}" id="verP"> <i class="fa fa-eye" title="Ver Paciente"></i></a>
                    | <a href="{{route('personas.edit', $persona->id)}}" id="editarP"> <i class="fa fa-pencil" title="Editar Paciente"></i></a>
                        {{-- <input class="btn btn-link" type="submit" name="btnLink" value= ''> --}}
                      | <a href="{{route('persona.delete', $persona->id)}}" id="deletePaciente"><i class="fa fa-remove" title="Eliminar Paciente"></i>  </a>
                    {{-- | <button type="submit" class="fa fa-remove" title="Eliminar Paciente"></button> --}}
                    |<a href="{{route('agenda.create',$persona->id)}}"> <i class="fa fa-calendar" title="Asignar Turno"></i> </a>
                    | <a href="{{route('fichaPersonal.show', $persona->id)}}"> <i class="fa fa-archive" title="Ficha de {{$persona->pers_nomb}}"></i> </a>

                      {{-- </form> --}}
                    </td>

                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nombre
                    </th>
                    <th>Apellido
                    </th>
                    <th>DNI
                    </th>
                    <th>CUIT
                    </th>
                    <th>Domicilio
                    </th>
                    <th>Telefono
                    </th>
                    <th>Opciones

                    </th>
                    {{-- <th>Imprimir

                    </th> --}}
                </tr>
            </tfoot>
        </table>
        <div class="">
          <a href="{{route('nuevoPaciente')}}" class="btn btn-primary btn-sm"> Nuevo</a>
        </div>
    </section>
</div>

@endsection
