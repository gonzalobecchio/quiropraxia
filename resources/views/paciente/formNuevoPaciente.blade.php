@extends('layouts.app')

@section('alerts')
@if ($errors->any())
<div class="alert alert-warning" role="alert">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>

@endif
@endsection

@section('content')

<form action="{{route('personas.store')}}" method="post" enctype="multipart/form-data">
  @csrf
  <div class="container">
    <section>
      <span class="section">Informacion personal paciente</span>
      @yield('alerts')
      <div class="row">
        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="fullname">Nombre * :</label>
          <input type="text" id="fullname" class="form-control"  maxlength="30" name="nombre" value="{{old('nombre')}}">
        </div>

        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="apellido">Apellido * : </label>
          <input type="text" id="apellido" class="form-control" maxlength="30" name="apellido" value="{{old('apellido')}}">
        </div>
        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="dni">DNI (opcional) :</label>
          <input type="text" id="dni" class="form-control" maxlength="8" name="dni" value="{{old('dni')}}">
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 col-sm-4  form-group has-feedback">
          <label for="dni">Estado Civil (opcional) :</label>
          <select class="form-control" name="esci">
            <option value='Soltero'> Soltero </option>
            <option value="Casado"> Casado</option>
            <option value="Separado"> Separado</option>
            <option value="Divorciado"> Divorciado</option>
            <option value="Viudo">Viudo</option>

          </select>
        </div>
        <div class="col-md-2 col-sm-2  form-group has-feedback">
          <label for="cuit">Cant. de hijos (opcional) :</label>
          <input type="number" min="0" max="99" pattern="^[0-9]{1,}$"  value="0" name="cahi" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Cantidad de Hijos">

        </div>
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="domicilio">Domicilio (opcional) :</label>
          <input type="text" id="domicilio" class="form-control" maxlength="30" name="domicilio" value="{{old('domicilio')}}">
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="telefono">Telefono (opcional) :</label>
          <input type="text" id="telefono" pattern="^[0-9]{1,}$" class="form-control" maxlength="10" name="telefono" value="{{old('telefono')}}">
        </div>

        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="email">Email (opcional) ejemplo@email.com :</label>
          <input type="text" id="email" class="form-control" maxlength="60" name="email" value="{{old('email')}}">
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">CUIT (opcional) :</label>
          <input type="text" id="cuit"  class="form-control" maxlength="11" name="cuit" value="{{old('cuit')}}">
        </div>

        <div class="col-md-6 col-sm-6  form-group">
          <label for="cuit">Fecha de Nacimiento (opcional) :</label>
          <input type="date" class="form-control" name="nacimiento" value="{{old('nacimiento')}}">


        </div>
      </div>

      <div class="row">


        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">Ocupacion (opcional) :</label>
          <input type="text" class="form-control" id="inputSuccess3" maxlength="30" name="ocupacion" value="{{old('ocupacion')}}">

        </div>
      </div>

      <div class="row">



      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">Provincia (opcional) :</label>
          <select class="form-control" id="provincia" name="provincia">
            @foreach ($provincias as $provincia)
              <option value="{{$provincia->id}}">{{$provincia->prov_nomb}}</option>
            @endforeach
          </select>

        </div>

        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="cuit">Localidad (opcional) :</label>
          <input type="text" class="form-control" name="localidad" value="{{old('localidad')}}">
          {{-- <select class="form-control" id="localidad" name="localidad">
            @foreach ($localidades as $localidad)
              <option value="{{$localidad->id}}">{{$localidad->loca_nomb}}</option>
            @endforeach

          </select> --}}

        </div>
      </div>

    </section>

    <span class="section"> </span>
    <section>
      <span class="section">Datos a completar por el profesional</span>

      <div class="row">
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="">¿Recibio cuidado quiropractico anteriormente? ¿Cuando y con quien? (opcional) </label>
          <textarea id="message" class="form-control" name="obs1" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('obs1')}}</textarea>
        </div>
        <div class="col-md-6 col-sm-6  form-group has-feedback">
          <label for="">Recomendado por (opcional) </label>
          <textarea id="message" class="form-control" name="recomendado" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('recomendado')}}</textarea>
        </div>

      </div>

      <div class="row">
        <div class="col-md-6 form-group has-feedback">
          <label for="">Cirujias (opcional) </label>
          <textarea id="message" class="form-control" name="cirujias" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('cirujias')}}</textarea>
        </div>
        <div class="col-md-6  form-group has-feedback">
          <label for="">Fracturas (opcional) </label>
          <textarea id="message" class="form-control" name="fracturas" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('fracturas')}}</textarea>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 form-group has-feedback">
          <label for="">Medicamentos (opcional) </label>
          <textarea id="message" class="form-control" name="medicamentos" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('medicamentos')}}</textarea>
        </div>
        <div class="col-md-6 form-group has-feedback">
          <label for="">Observaciones Profesional (opcional) </label>
          <textarea id="message" class="form-control" name="obs2" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('obs2')}}</textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 form-group has-feedback">
          <label for="">Referencias del paciente (opcional) </label>
          <textarea id="message" class="form-control" name="obs3" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{old('obs3')}}</textarea>
        </div>
      </div>
    </section>


  </div>
  {{-- <span class="section">Imagenes</span>
  <div class="row">
    <div class="col-2">
      <input type="file" name="file[]" value="" multiple>
    </div>
  </div> --}}

  <span class="section"> </span>
  <div class="">
    <input class="btn btn-primary btn-sm" type="submit" name="" value="Agregar">

  </div>


</form>

@endsection
