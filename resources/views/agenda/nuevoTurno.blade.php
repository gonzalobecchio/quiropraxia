@extends('layouts.app')
@section('content')
<div class="container">
    {{-- Modal Agregar Evento --}}
    <div class="">
        <div id="modal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Turno</h5>


                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">

                            <label for="">Fecha Turno</label>
                            <input  class="form-control"type="date" name="fechaTurno" value="" id="fechaTurno" readonly><br>
                            <label for="">Horario</label>
                            <input class="form-control" type="time" name="horarioInicio" value="08:00" id="horarioInicio" min="08:00" max="22:00" step="900" required><br>
                            {{-- <label for="">Horario Fin</label> --}}
                            <input type="hidden" name="horarioFin" value="" id="horarioFin"><br>
                            {{-- id Paciente --}}
                            <input class="form-control" id="id_persona" type="hidden" name="id" value="{{$id}}"><br>
                            {{-- ID Evento --}}
                            {{-- <input class="form-control" type="text" name="" value="" id="idEvento"><br> --}}
                        </form>



                    </div>
                    <div class="modal-footer">
                        <button id="agendar" type="button" class="btn btn-primary" data-dismiss="modal">Agendar</button>
                        <button id="cancelar" type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                        {{-- <button type="button" class="btn btn-danger">Eliminar</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Editar y Eliminar --}}
    <div class="">
        <div id="modalEditarEliminar" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5  id="title_UD" class="modal-title"> Editando/Eliminado</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-group">

                            <label for="">Fecha Turno</label>
                            <input  class="form-control"type="date" name="" value="" min="" id="fechaTurnoEM" required><br>
                            <label for="">Horario</label>
                            <input class="form-control" type="time" name="" value="" id="horarioInicioEM" min="08:00" max="22:00" step="900" required><br>
                            {{-- <label for="">Horario Fin</label> --}}
                            {{-- <input type="hidden" name="" value="" id="horarioFinEM"><br> --}}
                            {{-- id Paciente --}}
                            <input class="form-control" id="id_personaEM" type="hidden" name="id" value="{{$id}}"><br>
                            {{-- ID Evento --}}
                            <input class="form-control" type="hidden" name="" value="" id="idEventoEM"><br>
                        </form>



                    </div>
                    <div class="modal-footer">
                        <button id="eliminar" type="button" class="btn btn-primary" data-dismiss="modal">Eliminar</button>
                        <button id="editar" type="button" class="btn btn-success" data-dismiss="modal">Editar</button>
                        <button id="cancelar" type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                        {{-- <button type="button" class="btn btn-danger">Eliminar</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section>
        <span class="section">Calendario de Turnos</span>
        <div id='calendar'>
        </div>

    </section>
</div>
@endsection
