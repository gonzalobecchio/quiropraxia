@extends('layouts.app')

@section('content')

@section('alerts')


@endsection

<span class="section">Imagenes de {{$persona->pers_nomb . " " . $persona->pers_apel}} </span>
{{-- {{dd($imagens)}} --}}
<section>
    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                {{-- <th class="th-sm">#
                </th> --}}
                <th class="th-sm">Nombre
                </th>
                {{-- <th class="th-sm">Path
                </th> --}}
                <th class="th-sm">Extension
                </th>
                <th class="th-sm">Creacion
                </th>
                <th class="th-sm">Tamaño
                </th>
                <th class="th-sm">Opciones
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($imagens as $imagen)
            <tr>
                {{-- <td><a href="{{$imagen->id}}"></a>{{$imagen->id}}</td> --}}
                {{-- <td> <a href="{{route('imagen.show',$imagen->id)}}">{{$imagen->imag_nombre}}</a></td> --}}
                <td><a href={{asset($imagen->imag_ruta)}}>{{$imagen->imag_nombre}}</a> </td>
                {{-- <td>{{$imagen->imag_ruta}}</td> --}}
                <td>{{$imagen->imag_exte}}</td>
                <td>{{$imagen->created_at}}</td>
                <td>{{round((int)$imagen->imag_size / 1024,2)}} KB</td>
                <td>
                    <a href="{{route('imagen.show',$imagen->id)}}" title="Visualizar Imagen" id="modalImagen"><i class="fa fa-eye"></i></a> |
                    {{-- <a href="{{route('imagen.show',$imagen->id)}}" title="Visualizar Imagen" id="modalImagen"><i class="fa fa-eye"></i></a> | --}}
                    <a href="{{route('imagen.delete', $imagen->id)}}" title="Eliminar Imagen" id="deleteImg"> <i class="fa fa-remove"></i></a>
                    {{-- <button id="btn" type="button" name="button">Presionar</button> --}}
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                {{-- <th class="th-sm">#
                </th> --}}
                <th class="th-sm">Nombre
                </th>
                {{-- <th class="th-sm">Path
                </th> --}}
                <th class="th-sm">Extension
                </th>
                <th class="th-sm">Creacion
                </th>
                <th class="th-sm">Tamaño
                </th>
                <th class="th-sm">Opciones
                </th>
            </tr>
        </tfoot>
    </table>

    {{-- <form id="formulario" action="{{route('imagen.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file" id="file">
    <input type="hidden" name="persona_id" value="{{$persona->id}}">
    <br>
    <br>
    <input class="btn btn-primary" type="submit" name="" value="Cargar"> |
    <a href="{{route('fichaPersonal.show',$persona->id)}}">Volver a Fichas</a>
    </form> --}}


    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#basicExampleModal">
        Cargar Imagen
    </button>
    <a href="{{route('fichaPersonal.show',$persona->id)}}" class="btn btn-primary btn-sm">Volver a Fichas</a>

    <!-- Modal -->
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cargando Imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formulario" action="{{route('imagen.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Observacion *</label>
                            <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" name="observacion" required></textarea>
                        </div>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" lang="es" name="file" id="file" required>
                            <label class="custom-file-label" for="file">Seleccionar Archivo</label>
                            <input type="hidden" name="persona_id" value="{{$persona->id}}">
                        </div>


                        <div class="modal-footer">
                            <input class="btn btn-primary" type="submit" name="" value="Cargar">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>
                {{-- <div class="modal-footer">

        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
            </div>
        </div>
    </div>
</section>

{{-- <a href="{{route('imagen.create')}}"> Agregar Imagen </a> --}}






@endsection
