@extends('layouts.app')

@section('content')


<section>
    <span class="section">Imagen {{$imagen->imag_nombre}}</span>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="title_left">
                    <h2>Observacion Imagen</h2>
                </div>
                <p>{{$imagen->imag_obse}}</p>


            </div>
            <div class="col-md-8">
                <img src="{{asset($imagen->imag_ruta)}}" class="img-fluid img-thumbnail" alt="Responsive image">

                {{-- <iframe class="embed-responsive embed-responsive-1by1" src="" allowfullscreen></iframe> --}}

            </div>

        </div>
    </div>
</section>
&nbsp;
&nbsp;
<div class="row">
    <div class="col">
        <a href="{{route('imagen.index',$imagen->persona_id)}}">Volver</a>
    </div>


</div>



@endsection
