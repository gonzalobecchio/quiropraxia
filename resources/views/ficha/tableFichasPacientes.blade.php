@extends('layouts.app')
@section('content')
<div class="container">
    <section>
        <span class="section">Fichas de pacientes</span>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="th-sm">Nombre
                    </th>
                    <th class="th-sm">Apellido
                    </th>
                    <th class="th-sm">DNI
                    </th>
                    <th class="th-sm">CUIT
                    </th>
                    <th class="th-sm">Domicilio
                    </th>
                    <th class="th-sm">Telefono
                    </th>
                    <th class="th-sm">Opciones

                    </th>

                </tr>
            </thead>
            <tbody>
                @foreach ($personas as $persona)
                <tr>

                    <td>{{$persona->pers_nomb}}</td>
                    <td>{{$persona->pers_apel}}</td>
                    <td>{{$persona->pers_dni}}</td>
                    <td>{{$persona->pers_cuit}}</td>
                    <td>{{$persona->pers_domi}}</td>
                    <td>{{$persona->pers_tele}}</td>
                    <td>
                     <a href="{{route('fichaPersonal.show', $persona->id)}}"> <i class="fa fa-archive" title="Ficha de {{$persona->pers_nomb}}"></i> </a>
                     </a>
                    </td>



                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nombre
                    </th>
                    <th>Apellido
                    </th>
                    <th>DNI
                    </th>
                    <th>CUIT
                    </th>
                    <th>Domicilio
                    </th>
                    <th>Telefono
                    </th>
                    <th>Opciones

                    </th>
                </tr>
            </tfoot>
        </table>
    </section>

</div>

@endsection
