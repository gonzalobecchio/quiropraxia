@extends('layouts.app')

@section('content')
<section>
    <span class="section">Controles de {{$persona->pers_nomb . ' ' .$persona->pers_apel}} </span>
    <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">Fecha Creacion
                </th>
                <th class="th-sm">Atendido por
                </th>
                <th class="th-sm">Observacion Adicional
                </th>

                <th class="th-sm">Opciones
                </th>
                {{-- <th class="th-sm">Ver
                </th>
                <th class="th-sm">Editar
                </th> --}}
            </tr>
        </thead>
        <tbody>
            {{-- @foreach ($userControl as $profesional ) --}}

            {{-- {{$controlPersona}} --}}
            @foreach ($controlPersona as $controles)
              {{-- {{$controles->id}} --}}
              <tr>
                  <td>{{$controles->created_at}}</td>
                  <td>{{$controles->user->name}}</td>
                  <td class="text-center" id="observacionV"> @if (is_null($controles->observacionAdicional))
                  <a id="{{$controles->id}}" href="#!"> *** Sin observacion *** </a>
                @else <a id="{{$controles->id}}" href="#!">  {{$controles->observacionAdicional}} </a> </td>  @endif
                  <td>


                    {{-- <form action="{{route('control.delete', $controles->id)}}" method="post">
                      @method('delete')
                      @csrf --}}
                      {{session(['persona_id' => $persona->id])}}
                      {{-- <input type="hidden" name="persona_id" value="{{$persona->id}}"> --}}
                      <a href="{{route('control.edit', [$controles->control_id,'ver'])}}"><i class="fa fa-eye" title="Visualizar Control"></i></a> |
                      <a href="{{route('control.edit', [$controles->control_id])}}"><i class="fa fa-pencil" title="Editar Control"></i></a>
                    | <a href="{{route('control.delete', $controles->id)}}" id="deleteControl"><i class="fa fa-remove" title="Eliminar Control"></i></a>

                      {{-- <button class="fa fa-remove" type="submit" title="Eliminar Control"></button> --}}

                    {{-- </form> --}}
                    {{-- <a href="#"><i class="fa fa-remove" title="Eliminar Control"></i></a> --}}
                  </td>

              </tr>
            @endforeach
            {{-- @endforeach --}}

        </tbody>
        <tfoot>
          <tr>
              <th class="th-sm">Fecha Creacion
              </th>
              <th class="th-sm">Atendido por
              </th>
              <th class="th-sm">Observacion Adicional
              </th>
              <th class="th-sm">Opciones
              </th>
              {{-- <th class="th-sm">Ver
              </th>
              <th class="th-sm">Editar
              </th> --}}
          </tr>
        </tfoot>
    </table>
    <div class="">
        <a href="{{route('control.create',$persona->id)}}" class="btn btn-primary btn-sm">Agregar Control</a>
        <a href="{{route('fichaPersonal.show',$persona->id)}}" class="btn btn-primary btn-sm">Volver a Fichas</a>
    </div>
</section>

{{-- Modal --}}
<div class="modal" tabindex="-1" role="dialog" id="modalObseV">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5  id="titleObse" class="modal-title">Observacion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label for="observacionTabla">Ingrese observacion: (opcional)</label>
        <textarea id="textoModal" class="form-control" name="observacionTabla" rows="8" cols="80"></textarea>
      </div>
      <div class="modal-footer">
          <input type="hidden" id="id_observacion" name="id_observacion" value="">
          <button  id="guardarObservacion" type="button" class="btn btn-primary">Guardar</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection
