@extends('layouts.app')

@section('content')
<span class="section">Nuevo Control de {{$persona->pers_nomb . ' ' . $persona->pers_apel }} </span>
<section>
    <form class="" action="{{route('control.store')}}" method="POST">
      @csrf
      <input type="hidden" name="persona_id" value="{{$persona->id}}">
        <div class="row">
            <div class="col">
                {{-- tabla 1 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Piernas Supino</th>
                            <th colspan="2" scope="col">Piernas Prono</th>
                            <th colspan="2" scope="col">Giro Cab</th>
                            <th colspan="2" scope="col">Pelvis</th>
                            <th colspan="2" scope="col">Rest Sacra</th>
                            <th colspan="2" scope="col">Fosas</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>

                        </tr>
                        <tr>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat" type="checkbox" name="x_supino_izq_1" value="Izq">
                                    <input class="flat" type="checkbox" name="x_supino_izq_2" value="Izq">
                                    <input class="flat" type="checkbox" name="x_supino_izq_3" value="Izq">
                                    <input class="flat" type="checkbox" name="x_supino_izq_4" value="Izq">
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_supino_der_1" value="Der">
                                    <input class="flat" type="checkbox" name="x_supino_der_2" value="Der">
                                    <input class="flat" type="checkbox" name="x_supino_der_3" value="Der">
                                    <input class="flat" type="checkbox" name="x_supino_der_4" value="Der">
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat" type="checkbox" name="x_prono_izq_1" value="Izq">
                                    <input class="flat" type="checkbox" name="x_prono_izq_2" value="Izq">
                                    <input class="flat" type="checkbox" name="x_prono_izq_3" value="Izq">
                                    <input class="flat" type="checkbox" name="x_prono_izq_4" value="Izq">
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_prono_der_1" value="Der">
                                    <input class="flat" type="checkbox" name="x_prono_der_2" value="Der">
                                    <input class="flat" type="checkbox" name="x_prono_der_3" value="Der">
                                    <input class="flat" type="checkbox" name="x_prono_der_4" value="Der">
                                </div>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Der"></td>
                            <td> <select class=""class="" name="fosa_izq">
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>

                            </td>
                            <td> <select class=""class="" name="fosa_der">
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        &nbsp;
        &nbsp;
        <div class="row">
            <div class="col">
                <h6>Atlas</h6>
                <select class="form-control"class="select2_multiple form-control" multiple="multiple" name="AtAx[]">
                    @foreach ($AtAx as $AtAx )
                      <option value="{{$AtAx->id}}"> {{$AtAx->name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Axis</h6>
                <select class="form-control"class="select2_multiple form-control" multiple="multiple" name="Axis[]">
                    @foreach ($Axis as $axis)
                      <option value="{{$axis->id}}">{{$axis->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <h6>Segmento</h6>
                <select class="form-control"class="select2_multiple form-control" multiple="multiple" name="Segmento[]">
                    @foreach ($Segmentos as $segmento)
                      <option value="{{$segmento->id}}">{{$segmento->name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Delineamiento</h6>
                <select class="form-control"class="select2_multiple form-control" multiple="multiple" name="Delineamiento[]">
                    @foreach ($Delineamiento as $delineamiento)
                      <option value="{{$delineamiento->id}}">{{$delineamiento->name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Pelvis</h6>
                <select class="form-control"class="select2_multiple form-control" multiple="multiple" name="Pelvis[]">
                    @foreach ($Pelvis as $pelvis)
                      <option value="{{$pelvis->id}}">{{$pelvis->name}}</option>
                    @endforeach


                </select>
            </div>

        </div>
        &nbsp;
        &nbsp;
        <div class="row">

            <div class="col-6">
                <h6>Palpacion</h6>
                <textarea id="message" class="form-control" name="cont_palp" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
            </div>
            <div class="col-6">
              <h6>Observacion Control</h6>
              <textarea id="message" class="form-control" name="observacionControl" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
                {{-- <h6>Pre/Post</h6>
                <select class="form-control" name="cont_pp">
                    <option value="Pre">Pre</option>
                    <option value="Post">Post</option>

                </select> --}}
            </div>
            {{-- <div class="col-3">
                <h6>Ajuste</h6>
                <select class="form-control" name="cont_ajus">
                    <option>S/P</option>
                    <option>T/G</option>
                    <option>F/S</option>
                    <option>D</option>
                    <option>G</option>
                    <option>T/H</option>
                    <option>P</option>
                </select>
            </div> --}}
        </div>
        &nbsp;
        &nbsp;
        <div class="row">
            <div class="col">
                <input class="btn btn-primary btn-sm" type="submit" name="" value="Guardar">
              <a href="{{route('control.index', $persona->id)}}" class="btn btn-primary btn-sm">volver a control</a>

            </div>
        </div>


    </form>

</section>


@endsection
