@extends('layouts.app')

@section('content')
<span class="section"> @if ($form == 'ver')
  Visualizando Control de @foreach ($control->personas as $persona) {{$persona->pers_nomb . ' ' . $persona->pers_apel . ' - Fecha ' . $control->created_at}} @endforeach
@else
  Editando Control de  @foreach ($control->personas as $persona) {{$persona->pers_nomb . ' ' . $persona->pers_apel . ' - Fecha ' . $control->created_at}} @endforeach
@endif  </span>
<section>
    <form class="" action="{{route('control.update', $control->id)}}" method="POST">
      @csrf
      @method('PUT')
      <input type="hidden" name="persona_id" @foreach ($control->personas as $persona) value="{{$persona->id}}"  @endforeach>
        <div class="row">
            <div class="col">
                {{-- tabla 1 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Piernas Supino</th>
                            <th colspan="2" scope="col">Piernas Prono</th>
                            <th colspan="2" scope="col">Giro Cab</th>
                            <th colspan="2" scope="col">Pelvis</th>
                            <th colspan="2" scope="col">Rest Sacra</th>
                            <th colspan="2" scope="col">Fosas</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>

                        </tr>
                        <tr>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat" type="checkbox" name="x_supino_izq_1" value="Izq" @if ($control->x_supino_izq_1 == 'Izq') checked @endif>
                                    <input class="flat" type="checkbox" name="x_supino_izq_2" value="Izq" @if ($control->x_supino_izq_2 == 'Izq') checked @endif>
                                    <input class="flat" type="checkbox" name="x_supino_izq_3" value="Izq" @if ($control->x_supino_izq_3 == 'Izq') checked @endif>
                                    <input class="flat" type="checkbox" name="x_supino_izq_4" value="Izq" @if ($control->x_supino_izq_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_supino_der_1" value="Der" @if ($control->x_supino_der_1 == 'Der') checked @endif>
                                    <input class="flat" type="checkbox" name="x_supino_der_2" value="Der" @if ($control->x_supino_der_2 == 'Der') checked @endif>
                                    <input class="flat" type="checkbox" name="x_supino_der_3" value="Der" @if ($control->x_supino_der_3 == 'Der') checked @endif>
                                    <input class="flat" type="checkbox" name="x_supino_der_4" value="Der" @if ($control->x_supino_der_4 == 'Der') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat" type="checkbox" name="x_prono_izq_1" value="Izq" @if ($control->x_prono_izq_1 == 'Izq') checked @endif>
                                    <input class="flat" type="checkbox" name="x_prono_izq_2" value="Izq" @if ($control->x_prono_izq_2 == 'Izq') checked @endif>
                                    <input class="flat" type="checkbox" name="x_prono_izq_3" value="Izq" @if ($control->x_prono_izq_3 == 'Izq') checked @endif>
                                    <input class="flat" type="checkbox" name="x_prono_izq_4" value="Izq" @if ($control->x_prono_izq_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_prono_der_1" value="Der"  @if ($control->x_prono_der_1 == 'Der') checked @endif>
                                    <input class="flat" type="checkbox" name="x_prono_der_2" value="Der"  @if ($control->x_prono_der_2 == 'Der') checked @endif>
                                    <input class="flat" type="checkbox" name="x_prono_der_3" value="Der"  @if ($control->x_prono_der_3 == 'Der') checked @endif>
                                    <input class="flat" type="checkbox" name="x_prono_der_4" value="Der"  @if ($control->x_prono_der_4 == 'Der') checked @endif>
                                </div>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Izq"  @if ($control->giro_cab == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Der"  @if ($control->giro_cab == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Izq"  @if ($control->pelvis == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Der"  @if ($control->pelvis == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Izq"  @if ($control->rest_sacra == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Der"  @if ($control->rest_sacra == 'Der') checked @endif></td>
                            <td> <select class="" name="fosa_izq">
                                    <option value="{{$control->fosa_izq}}">{{$control->fosa_izq}}</option>
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>

                            </td>
                            <td> <select class="" name="fosa_der">
                                    <option value="{{$control->fosa_der}}">{{$control->fosa_der}}</option>
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        &nbsp;
        &nbsp;
        <div class="row">
            <div class="col">
              {{-- {{dd($id)}} --}}
                <h6>Atlas</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="AtAx[]">
                  @foreach ($AtAx as $atlas)
                            <option
                            @foreach ($atlas->controls as $controls)
                              @if ($controls->pivot->control_id == $id)
                                selected
                              @endif
                            @endforeach value="{{$atlas->id}}">{{$atlas->name}}
                          </option>
                  @endforeach
                </select>
            </div>
            <div class="col">
                <h6>Axis</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Axis[]">
                  @foreach ($Axis as $axis)
                            <option
                            @foreach ($axis->controls as $controls)
                              @if ($controls->pivot->control_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$axis->id}}">{{$axis->name}}
                          </option>
                  @endforeach
                </select>
            </div>
            <div class="col">
                <h6>Segmento</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Segmento[]">
                  @foreach ($Segmentos as $segmentos)
                            <option
                            @foreach ($segmentos->controls as $controls)
                              @if ($controls->pivot->control_id == $id)
                                selected
                              @endif
                            @endforeach value="{{$segmentos->id}}">{{$segmentos->name}}
                          </option>
                  @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Delineamiento</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Delineamiento[]">
                  @foreach ($Delineamientos as $delineamiento)
                            <option
                            @foreach ($delineamiento->controls as $controls)
                              @if ($controls->pivot->control_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$delineamiento->id}}">{{$delineamiento->name}}
                          </option>
                  @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Pelvis</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Pelvis[]">
                  @foreach ($Pelvis as $pelvis)
                            <option
                            @foreach ($pelvis->controls as $controls)
                              @if ($controls->pivot->control_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$pelvis->id}}">{{$pelvis->name}}
                          </option>
                  @endforeach

                </select>
            </div>

        </div>
        &nbsp;
        &nbsp;
        <div class="row">
            <div class="col-6">
                <h6>Palpacion</h6>
                {{-- {{(dd($control))}} --}}
                <textarea id="message" class="form-control" name="cont_palp" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">  {{$control->cont_palp}}</textarea>
            </div>
            <div class="col-6">
                <h6>Pre/Post</h6>
                {{-- <h6>Observacion Control</h6> --}}
                <textarea id="message" class="form-control" name="observacionControl" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10">{{$control->ObservacionControl}}</textarea>
                {{-- <select class="form-control" name="cont_pp">
                    <option value="{{$control->cont_pp}}">{{$control->cont_pp}}</option>
                    <option value="Pre">Pre</option>
                    <option value="Post">Post</option>

                </select> --}}
            </div>
            {{-- <div class="col-3">
                <h6>Ajuste</h6>
                <select class="form-control" name="cont_ajus">
                    <option value="{{$control->cont_ajus}}">{{$control->cont_ajus}}</option>
                    <option>S/P</option>
                    <option>T/G</option>
                    <option>F/S</option>
                    <option>D</option>
                    <option>G</option>
                    <option>T/H</option>
                    <option>P</option>
                </select>
            </div> --}}
        </div>
        &nbsp;
        &nbsp;
        <div class="row">
            <div class="col">
              @if ($form == 'ver')
                <a href="{{route('control.edit',$control->id)}}" class="btn btn-primary btn-sm">Editar</a>
              @else
                <input class="btn btn-primary btn-sm" type="submit" name="" value="Guardar Cambios">
              @endif
              @foreach ($control->personas as $persona)   <a href="{{route('control.index', $persona->id)}}" class="btn btn-primary btn-sm">Volver a Control</a> @endforeach

            </div>
        </div>


    </form>

</section>


@endsection
