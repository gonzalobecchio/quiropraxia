@extends('layouts.app')
@section('content')
<div class="container">
    <section>
        <span class="section">Perfil de {{$persona->pers_nomb . ' ' . $persona->pers_apel}}</span>
    </section>
    <section>
        <div class="x_panel">
            <div class="row">
                <div class="col-4">
                    <div class="profile_img">
                        <div id="crop-avatar">
                            <!-- Current avatar -->
                            <img class="img-responsive avatar-view" src="{{asset('/img/user.png')}}" alt="Avatar">
                        </div>
                    </div>
                    &nbsp;
                    <ul class="list-unstyled user_data">
                        <li title="Domicilio">
                          @if (empty($persona->pers_domi)) <i class="fa fa-map-marker user-profile-icon"></i><a href="{{route('personas.edit',[$persona->id])}}"> Completar Datos </a>   @else  <i class="fa fa-map-marker user-profile-icon"> {{$persona->pers_domi}} </i> @endif
                        </li>

                        <li title="Telefono">
                             @if (empty($persona->pers_tele)) <i class="fa fa-phone user-profile-icon"></i><a href="{{route('personas.edit',[$persona->id])}}">  Completar Datos </a>  @else  <i class="fa fa-phone user-profile-icon"> {{$persona->pers_tele}} </i> @endif
                            {{-- <i class="fa fa-phone user-profile-icon"></i> {{$persona->pers_tele}} --}}
                        </li>

                        <li title="Email">
                            @if (empty($persona->pers_emai)) <i class="fa fa-at user-profile-icon"></i><a href="{{route('personas.edit',[$persona->id])}}"> Completar Datos </a>  @else  <i class="fa fa-at user-profile-icon"> {{$persona->pers_emai}} </i> @endif
                             {{-- <i class="fa fa-at user-profile-icon"></i> {{$persona->pers_emai}} --}}
                        </li>
                        <li title="Fecha Nacimiento">
                              @if (empty($persona->pers_naci)) <i class="fa fa-birthday-cake user-profile-icon"></i><a href="{{route('personas.edit',[$persona->id])}}"> Completar Datos </a>   @else  <i class="fa fa-birthday-cake user-profile-icon"> {{$persona->pers_naci}} </i> @endif
                             {{-- <i class="fa fa-birthday-cake user-profile-icon"></i> {{$persona->pers_naci}} --}}
                        </li>
                        <li title="Edad">
                            <i class="fa fa-user user-profile-icon"></i> {{$persona->pers_edad}} Años
                        </li>


                        {{-- <li class="m-top-xs">
                        <i class="fa fa-external-link user-profile-icon"></i>
                        <a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
                    </li> --}}
                    </ul>


                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                  <div class="row">
                                    <div class="col">
                                      <h5 class="card-title">Visitas</h5>
                                      <p class="card-text">
                                          <h1> {{$datosPerfilPaciente['visitas']}} </h1>
                                      </p>
                                    </div>
                                    <div class="col">
                                      Tabla con las visitas ordenadas por fecha, creacion de una nueva y edicion.
                                    </div>

                                  </div>

                                    <a href="{{route('visita.index', $persona->id)}}" class="btn btn-primary btn-sm">Ir a Visitas</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                  <div class="row">
                                      <div class="col">
                                        <h5 class="card-title">Patron</h5>
                                        <p class="card-text">
                                            <h1>1</h1>
                                        </p>
                                      </div>
                                      <div class="col">
                                        Ficha con informacion del patron
                                      </div>

                                  </div>
                                  <a href="{{route('patron.edit', $persona->id)}}" class="btn btn-primary btn-sm">Ir a Patron</a>
                                    {{-- <a href="{{route('patron.index', $persona->id)}}" class="btn btn-primary">Ir a Patron</a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    &nbsp;
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                  <div class="row">
                                    <div class="col">
                                      <h5 class="card-title">Controles</h5>
                                      <p class="card-text">
                                          <h1> {{$datosPerfilPaciente['controles']}}</h1>
                                      </p>
                                    </div>
                                    <div class="col">
                                      Seccion de registros de los controles realizados por el profesional ordenados por fecha
                                    </div>
                                  </div>
                                  <a href="{{route('control.index', $persona->id)}}" class="btn btn-primary btn-sm">Ir a Control</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="card-title">Imagenes</h5>
                                            <p class="card-text">
                                                <h1>{{$datosPerfilPaciente['imagenes']}}</h1>
                                            </p>
                                        </div>
                                        <div class="col">
                                            Galeria de imagenes del paciente, contenido de fotos, radriografias, y material de utilidad para el profesional
                                        </div>
                                    </div>

                                    <a href="{{route('imagen.index', $persona->id)}}" class="btn btn-primary btn-sm">Ir a Imagenes</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>


</div>
{{-- <div class="row">
      <div class="col-sm-3">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Visitas</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="{{route('visita.index', $persona->id)}}" class="btn btn-primary">Ir a Visitas</a>
</div>
</div>
</div>
<div class="col-sm-3">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Patron</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="{{route('patron.index', $persona->id)}}" class="btn btn-primary">Ir a Patron</a>
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Control</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="{{route('control.index', $persona->id)}}" class="btn btn-primary">Ir a Control</a>
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Imagenes</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="{{route('imagen.index', $persona->id)}}" class="btn btn-primary">Ir a Imagenes</a>
        </div>
    </div>
</div>
</div> --}}

</section>
<a href="{{route('listadoPacientes')}}">Volver a Lista de Pacientes</a>

</div>

@endsection
