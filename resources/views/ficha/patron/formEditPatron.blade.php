@extends('layouts.app')


@section('content')
<section>
    <span class="section">Editar Patron</span>
    <form class="" action="{{route('patron.update', $id)}}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="persona_id" value="{{$id}}">
        <div class="row">
            <div class="col">
                <h6>Atlas</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="AtAx[]">
                  @foreach ($Atlas as $atlas)
                            <option
                            @foreach ($atlas->personas as $persona)
                              @if ($persona->pivot->persona_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$atlas->id}}">{{$atlas->name}}
                          </option>
                  @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Axis</h6>

                <select class="select2_multiple form-control" multiple="multiple" name="Axis[]">
                    @foreach ($Axis as $axis)
                              <option
                              @foreach ($axis->personas as $persona)
                                @if ($persona->pivot->persona_id == $id)
                                  selected
                                @endif
                              @endforeach  value="{{$axis->id}}">{{$axis->name}}
                            </option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <h6>Segmento</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Segmento[]">
                  @foreach ($Segmentos as $segmentos)
                            <option
                            @foreach ($segmentos->personas as $persona)
                              @if ($persona->pivot->persona_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$segmentos->id}}">{{$segmentos->name}}
                          </option>
                  @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Desealineamiento</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Delineamiento[]">
                  @foreach ($Delineamientos as $delineamiento)
                            <option
                            @foreach ($delineamiento->personas as $persona)
                              @if ($persona->pivot->persona_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$delineamiento->id}}">{{$delineamiento->name}}
                          </option>
                  @endforeach


                </select>
            </div>
            <div class="col">
                <h6>Pelvis</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Pelvis[]">
                  @foreach ($Pelvis as $pelvis)
                            <option
                            @foreach ($pelvis->personas as $persona)
                              @if ($persona->pivot->persona_id == $id)
                                selected
                              @endif
                            @endforeach  value="{{$pelvis->id}}">{{$pelvis->name}}
                          </option>
                  @endforeach


                </select>
            </div>


        </div>
        &nbsp;
        &nbsp;
        {{-- seccion tabla 1 --}}
        <div class="row">
            <div class="col">
                {{-- tabla 1 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Piernas Supino</th>
                            <th colspan="2" scope="col">Piernas Prono</th>
                            <th colspan="2" scope="col">Giro Cab</th>
                            <th colspan="2" scope="col">Pelvis</th>
                            <th colspan="2" scope="col">Rest Sacra</th>
                            <th colspan="2" scope="col">Fosas</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>

                        </tr>
                        <tr>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat"type="checkbox" name="x_supino_izq_1" value="Izq" @if ($patron->x_supino_izq_1 == 'Izq') checked @endif>
                                    <input class="flat"type="checkbox" name="x_supino_izq_2" value="Izq" @if ($patron->x_supino_izq_2 == 'Izq') checked @endif>
                                    <input class="flat"type="checkbox" name="x_supino_izq_3" value="Izq" @if ($patron->x_supino_izq_3 == 'Izq') checked @endif>
                                    <input class="flat"type="checkbox" name="x_supino_izq_4" value="Izq" @if ($patron->x_supino_izq_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat"type="checkbox" name="x_supino_der_1" value="Der" @if ($patron->x_supino_der_1 == 'Der') checked @endif>
                                    <input class="flat"type="checkbox" name="x_supino_der_2" value="Der" @if ($patron->x_supino_der_2 == 'Der') checked @endif>
                                    <input class="flat"type="checkbox" name="x_supino_der_3" value="Der" @if ($patron->x_supino_der_3 == 'Der') checked @endif>
                                    <input class="flat"type="checkbox" name="x_supino_der_4" value="Der" @if ($patron->x_supino_der_4 == 'Der') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat"type="checkbox" name="x_prono_izq_1" value="Izq" @if ($patron->x_prono_izq_1 == 'Izq') checked @endif>
                                    <input class="flat"type="checkbox" name="x_prono_izq_2" value="Izq" @if ($patron->x_prono_izq_2 == 'Izq') checked @endif>
                                    <input class="flat"type="checkbox" name="x_prono_izq_3" value="Izq" @if ($patron->x_prono_izq_3 == 'Izq') checked @endif>
                                    <input class="flat"type="checkbox" name="x_prono_izq_4" value="Izq" @if ($patron->x_prono_izq_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat"type="checkbox" name="x_prono_der_1" value="Der" @if ($patron->x_prono_der_1 == 'Der') checked @endif>
                                    <input class="flat"type="checkbox" name="x_prono_der_2" value="Der" @if ($patron->x_prono_der_1 == 'Der') checked @endif>
                                    <input class="flat"type="checkbox" name="x_prono_der_3" value="Der" @if ($patron->x_prono_der_1 == 'Der') checked @endif>
                                    <input class="flat"type="checkbox" name="x_prono_der_4" value="Der" @if ($patron->x_prono_der_1 == 'Der') checked @endif>
                                </div>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Izq" @if ($patron->giro_cab == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Der" @if ($patron->giro_cab == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Izq" @if ($patron->pelvis == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Der" @if ($patron->pelvis == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Izq" @if ($patron->rest_sacra == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Der" @if ($patron->rest_sacra == 'Der') checked @endif></td>
                            <td> <select class="" name="fosa_izq">
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value=",0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>

                            </td>
                            <td> <select class="" name="fosa_der">
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value=",0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        &nbsp;
        &nbsp;

        <span class="section">Palpacion de Muscular</span>
        {{-- seccion tabla 2 --}}
        <div class="row">
            <div class="col">
                {{-- Tabla 2 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Angular Omop</th>
                            <th colspan="2" scope="col">Oblicuo Men. Sup</th>
                            <th colspan="2" scope="col">Oblicuo May. Inf</th>
                            <th colspan="2" scope="col">Ecom</th>
                            <th colspan="2" scope="col">Com ></th>
                            <th colspan="2" scope="col">Recto Post > </th>
                            <th colspan="2" scope="col">Recto Post < </th>
                            <th colspan="2" scope="col">Inter Trans</th>
                            <th colspan="2" scope="col">Esplenio cab</th>
                            <th colspan="2" scope="col">Esplenio Cuello</th>
                            <th colspan="2" scope="col">Esc Medio</th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>




                        </tr>
                        <tr>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="angular_omop" value="Izq" @if ($patron->angular_omop == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="angular_omop" value="Der" @if ($patron->angular_omop == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_men" value="Izq" @if ($patron->oblicuo_men == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_men" value="Der" @if ($patron->oblicuo_men == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_may" value="Izq" @if ($patron->oblicuo_men == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_may" value="Der" @if ($patron->oblicuo_men == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="ecom" value="Izq" @if ($patron->ecom == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="ecom" value="Der" @if ($patron->ecom == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="com" value="Izq" @if ($patron->com == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="com" value="Der" @if ($patron->com == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_izq" value="Izq" @if ($patron->recto_post_izq == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_izq" value="Der" @if ($patron->recto_post_izq == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_der" value="Izq" @if ($patron->recto_post_der == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_der" value="Der" @if ($patron->recto_post_der == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="inter_trans" value="Izq" @if ($patron->inter_trans == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="inter_trans" value="Der" @if ($patron->inter_trans == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cab" value="Izq" @if ($patron->espelino_cab == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cab" value="Der" @if ($patron->espelino_cab == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cue" value="Izq" @if ($patron->espelino_cue == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cue" value="Der" @if ($patron->espelino_cue == 'Der') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="esc_medio" value="Izq" @if ($patron->esc_medio == 'Izq') checked @endif></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="esc_medio" value="Der" @if ($patron->esc_medio == 'Der') checked @endif></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>


        &nbsp;
        &nbsp;
        {{-- seccion tabla 3 Tablas vertebras --}}
        <span class="section">Transverso Espinso</span>
        <div class="row">
            {{-- C3-C7 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="C3" value="Izq" @if ($patron->C3 == 'Izq') checked @endif> </td>
                            <td>C3</td>
                            <td><input class="flat" type="radio" name="C3" value="Der" @if ($patron->C3 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C4" value="Izq" @if ($patron->C4 == 'Izq') checked @endif> </td>
                            <td>C4</td>
                            <td><input class="flat" type="radio" name="C4" value="Der" @if ($patron->C4 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C5" value="Izq" @if ($patron->C5 == 'Izq') checked @endif> </td>
                            <td>C5</td>
                            <td><input class="flat" type="radio" name="C5" value="Der" @if ($patron->C5 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C6" value="Izq" @if ($patron->C6 == 'Izq') checked @endif> </td>
                            <td>C6</td>
                            <td><input class="flat" type="radio" name="C6" value="Der" @if ($patron->C6 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C7" value="Izq" @if ($patron->C7 == 'Izq') checked @endif> </td>
                            <td>C7</td>
                            <td><input class="flat" type="radio" name="C7" value="Der" @if ($patron->C7 == 'Der') checked @endif></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            {{-- D1-D12 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="D1" value="Izq" @if ($patron->D1 == 'Izq') checked @endif> </td>
                            <td>D1</td>
                            <td><input class="flat" type="radio" name="D1" value="Der" @if ($patron->D1 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D2" value="Izq" @if ($patron->D2 == 'Izq') checked @endif> </td>
                            <td>D2</td>
                            <td><input class="flat" type="radio" name="D2" value="Der" @if ($patron->D2 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D3" value="Izq" @if ($patron->D3 == 'Izq') checked @endif> </td>
                            <td>D3</td>
                            <td><input class="flat" type="radio" name="D3" value="Der" @if ($patron->D3 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D4" value="Izq" @if ($patron->D4 == 'Izq') checked @endif> </td>
                            <td>D4</td>
                            <td><input class="flat" type="radio" name="D4" value="Der" @if ($patron->D4 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D5" value="Izq" @if ($patron->D5 == 'Izq') checked @endif> </td>
                            <td>D5</td>
                            <td><input class="flat" type="radio" name="D5" value="Der" @if ($patron->D5 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D6" value="Izq" @if ($patron->D6 == 'Izq') checked @endif> </td>
                            <td>D6</td>
                            <td><input class="flat" type="radio" name="D6" value="Der" @if ($patron->D6 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D7" value="Izq" @if ($patron->D7 == 'Izq') checked @endif> </td>
                            <td>D7</td>
                            <td><input class="flat" type="radio" name="D7" value="Der" @if ($patron->D7 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D8" value="Izq" @if ($patron->D8 == 'Izq') checked @endif> </td>
                            <td>D8</td>
                            <td><input class="flat" type="radio" name="D8" value="Der" @if ($patron->D8 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D9" value="Izq" @if ($patron->D9 == 'Izq') checked @endif> </td>
                            <td>D9</td>
                            <td><input class="flat" type="radio" name="D9" value="Der" @if ($patron->D9 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D10" value="Izq" @if ($patron->D10 == 'Izq') checked @endif> </td>
                            <td>D10</td>
                            <td><input class="flat" type="radio" name="D10" value="Der" @if ($patron->D10 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D11" value="Izq" @if ($patron->D11 == 'Izq') checked @endif> </td>
                            <td>D11</td>
                            <td><input class="flat" type="radio" name="D11" value="Der" @if ($patron->D11 == 'Der') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D12" value="Izq" @if ($patron->D12 == 'Izq') checked @endif> </td>
                            <td>D12</td>
                            <td><input class="flat" type="radio" name="D12" value="Der" @if ($patron->D12 == 'Der') checked @endif></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            {{-- L1-L5 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="L1" value="Izq" @if ($patron->L1 == 'Izq') checked @endif> </td>
                            <td>L1</td>
                            <td><input class="flat" type="radio" name="L1" value="Der" @if ($patron->L1 == 'Izq') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L2" value="Izq" @if ($patron->L2 == 'Izq') checked @endif> </td>
                            <td>L2</td>
                            <td><input class="flat" type="radio" name="L2" value="Der" @if ($patron->L2 == 'Izq') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L3" value="Izq" @if ($patron->L3 == 'Izq') checked @endif> </td>
                            <td>L3</td>
                            <td><input class="flat" type="radio" name="L3" value="Der" @if ($patron->L3 == 'Izq') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L4" value="Izq" @if ($patron->L4 == 'Izq') checked @endif> </td>
                            <td>L4</td>
                            <td><input class="flat" type="radio" name="L4" value="Der" @if ($patron->L4 == 'Izq') checked @endif></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L5" value="Izq" @if ($patron->L5 == 'Izq') checked @endif> </td>
                            <td>L5</td>
                            <td><input class="flat" type="radio" name="L5" value="Der" @if ($patron->L5 == 'Izq') checked @endif></td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
        &nbsp;
        &nbsp;
        {{-- seccion tabla 4  --}}
        <div class="row">

            <div class="col">
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Il Cost.</th>
                            <th colspan="2" scope="col">Dors. L</th>
                            <th colspan="2" scope="col">Ser P. Sup</th>
                            <th colspan="2" scope="col">Ser P. Inf</th>
                            <th colspan="2" scope="col">Lig SCMa.</th>
                            <th colspan="2" scope="col">Lig SCMe.</th>
                            <th colspan="2" scope="col">Lig Si</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>


                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="il_cost" value="Izq" @if ($patron->il_cost == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="il_cost" value="Der" @if ($patron->il_cost == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="dor_l" value="Izq" @if ($patron->dor_l == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="dor_l" value="Der" @if ($patron->dor_l == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_sup" value="Izq" @if ($patron->ser_p_sup == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_sup" value="Der" @if ($patron->ser_p_sup == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_inf" value="Izq" @if ($patron->ser_p_inf == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_inf" value="Der" @if ($patron->ser_p_inf == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMa" value="Izq" @if ($patron->SCMa == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMa" value="Der" @if ($patron->SCMa == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMe" value="Izq" @if ($patron->SCMe == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMe" value="Der" @if ($patron->SCMe == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="Lig_si" value="Izq" @if ($patron->Lig_si == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="Lig_si" value="Der" @if ($patron->Lig_si == 'Der') checked @endif> </td>

                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

        &nbsp;
        &nbsp;
        <span class="section">Palpacion de Movimiento</span>
        <div class="row">
            <div class="col-md-4">
                <label for="">Atlas</label>
                <textarea id="message" class="form-control" name="atlas_patr" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{$patron->atlas_patr}}</textarea>
            </div>
            <div class="col-md-4">
                <label for="">Axis</label>
                <textarea id="message" class="form-control" name="axis_patr" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{$patron->axis_patr}}</textarea>
            </div>
            <div class="col-md-4">
                <label for="">Inferiores</label>
                <textarea id="message" class="form-control" name="inferiores_patr" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{$patron->inferiores_patr}}</textarea>
            </div>

        </div>
        &nbsp;
        &nbsp;
        <div class="">
            <input class="btn btn-primary btn-sm" type="submit" name="" value="Guardar">
            <a href="{{route('fichaPersonal.show', $id)}}" class="btn btn-primary btn-sm">Volver a Fichas</a>
        </div>


    </form>

</section>

@endsection
