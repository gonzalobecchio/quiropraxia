@extends('layouts.app')

@section('content')
<section>
    <span class="section">Crear Patron</span>
    <form class="" action="{{route('patron.store', $id)}}" method="POST">
        @csrf
        <input type="hidden" name="persona_id" value="{{$id}}">
        <div class="row">
            <div class="col">
                <h6>Atlas</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="AtAx[]">
                    @foreach ($AtAx as $AtAx)
                        <option value="{{$AtAx->id}}"> {{$AtAx->name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Axis</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Axis[]">
                    @foreach ($Axis as $axis)
                      <option value="{{$axis->id}}">{{$axis->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <h6>Segmento</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Segmento[]">
                  @foreach ($Segmentos as $segmento)
                    <option value="{{$segmento->id}}">{{$segmento->name}}</option>
                  @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Delineamiento</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Delineamiento[]">
                  @foreach ($Delineamiento as $delineamiento)
                    <option value="{{$delineamiento->id}}">{{$delineamiento->name}}</option>
                  @endforeach

                </select>
            </div>
            <div class="col">
                <h6>Pelvis</h6>
                <select class="select2_multiple form-control" multiple="multiple" name="Pelvis[]">
                  @foreach ($Pelvis as $pelvis)
                    <option value="{{$pelvis->id}}">{{$pelvis->name}}</option>
                  @endforeach


                </select>
            </div>


        </div>
        &nbsp;
        &nbsp;
        {{-- seccion tabla 1 --}}
        <div class="row">
            <div class="col">
                {{-- tabla 1 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Piernas Supino</th>
                            <th colspan="2" scope="col">Piernas Prono</th>
                            <th colspan="2" scope="col">Giro Cab</th>
                            <th colspan="2" scope="col">Pelvis</th>
                            <th colspan="2" scope="col">Rest Sacra</th>
                            <th colspan="2" scope="col">Fosas</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>

                        </tr>
                        <tr>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat" type="checkbox" name="x_supino_izq_1" value="Izq">
                                    <input class="flat" type="checkbox" name="x_supino_izq_2" value="Izq">
                                    <input class="flat" type="checkbox" name="x_supino_izq_3" value="Izq">
                                    <input class="flat" type="checkbox" name="x_supino_izq_4" value="Izq">
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_supino_der_1" value="Der">
                                    <input class="flat" type="checkbox" name="x_supino_der_2" value="Der">
                                    <input class="flat" type="checkbox" name="x_supino_der_3" value="Der">
                                    <input class="flat" type="checkbox" name="x_supino_der_4" value="Der">
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat" type="checkbox" name="x_prono_izq_1" value="Izq">
                                    <input class="flat" type="checkbox" name="x_prono_izq_2" value="Izq">
                                    <input class="flat" type="checkbox" name="x_prono_izq_3" value="Izq">
                                    <input class="flat" type="checkbox" name="x_prono_izq_4" value="Izq">
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_prono_der_1" value="Der">
                                    <input class="flat" type="checkbox" name="x_prono_der_2" value="Der">
                                    <input class="flat" type="checkbox" name="x_prono_der_3" value="Der">
                                    <input class="flat" type="checkbox" name="x_prono_der_4" value="Der">
                                </div>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="giro_cab" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="pelvis" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="rest_sacra" value="Der"></td>
                            <td> <select class="" name="fosa_izq">
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>

                            </td>
                            <td> <select class="" name="fosa_der">
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        &nbsp;
        &nbsp;


        <span class="section"></span>
        {{-- seccion tabla 2 --}}
        <div class="row">
            <div class="col">
                {{-- Tabla 2 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Angular Omop</th>
                            <th colspan="2" scope="col">Oblicuo Men. Sup</th>
                            <th colspan="2" scope="col">Oblicuo May. Inf</th>
                            <th colspan="2" scope="col">Ecom</th>
                            <th colspan="2" scope="col">Com ></th>
                            <th colspan="2" scope="col">Recto Post ></th>
                            <th colspan="2" scope="col">Recto Post << /th>
                            <th colspan="2" scope="col">Inter Trans</th>
                            <th colspan="2" scope="col">Espelino cab</th>
                            <th colspan="2" scope="col">Espelino Cuello</th>
                            <th colspan="2" scope="col">Esc Medio</th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>




                        </tr>
                        <tr>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="angular_omop" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="angular_omop" value="Der" </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_men" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_men" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_may" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_may" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="ecom" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="ecom" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="com" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="com" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_izq" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_izq" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_der" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_der" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="inter_trans" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="inter_trans" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cab" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cab" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cue" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cue" value="Der"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="esc_medio" value="Izq"></td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="esc_medio" value="Der"></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>


        &nbsp;
        &nbsp;
        {{-- seccion tabla 3 Tablas vertebras --}}
        <div class="row">

            <span class="section">Transverso Espinso</span>

            {{-- C3-C7 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="C3" value="Izq"> </td>
                            <td>C3</td>
                            <td><input class="flat" type="radio" name="C3" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C4" value="Izq"> </td>
                            <td>C4</td>
                            <td><input class="flat" type="radio" name="C4" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C5" value="Izq"> </td>
                            <td>C5</td>
                            <td><input class="flat" type="radio" name="C5" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C6" value="Izq"> </td>
                            <td>C6</td>
                            <td><input class="flat" type="radio" name="C6" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C7" value="Izq"> </td>
                            <td>C7</td>
                            <td><input class="flat" type="radio" name="C7" value="Der"></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            {{-- D1-D12 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="D1" value="Izq"> </td>
                            <td>D1</td>
                            <td><input class="flat" type="radio" name="D1" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D2" value="Izq"> </td>
                            <td>D2</td>
                            <td><input class="flat" type="radio" name="D2" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D3" value="Izq"> </td>
                            <td>D3</td>
                            <td><input class="flat" type="radio" name="D3" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D4" value="Izq"> </td>
                            <td>D4</td>
                            <td><input class="flat" type="radio" name="D4" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D5" value="Izq"> </td>
                            <td>D5</td>
                            <td><input class="flat" type="radio" name="D5" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D6" value="Izq"> </td>
                            <td>D6</td>
                            <td><input class="flat" type="radio" name="D6" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D7" value="Izq"> </td>
                            <td>D7</td>
                            <td><input class="flat" type="radio" name="D7" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D8" value="Izq"> </td>
                            <td>D8</td>
                            <td><input class="flat" type="radio" name="D8" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D9" value="Izq"> </td>
                            <td>D9</td>
                            <td><input class="flat" type="radio" name="D9" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D10" value="Izq"> </td>
                            <td>D10</td>
                            <td><input class="flat" type="radio" name="D10" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D11" value="Izq"> </td>
                            <td>D11</td>
                            <td><input class="flat" type="radio" name="D11" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D12" value="Izq"> </td>
                            <td>D12</td>
                            <td><input class="flat" type="radio" name="D12" value="Der"></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            {{-- L1-L5 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="L1" value="Izq"> </td>
                            <td>L1</td>
                            <td><input class="flat" type="radio" name="L1" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L2" value="Izq"> </td>
                            <td>L2</td>
                            <td><input class="flat" type="radio" name="L2" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L3" value="Izq"> </td>
                            <td>L3</td>
                            <td><input class="flat" type="radio" name="L3" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L4" value="Izq"> </td>
                            <td>L4</td>
                            <td><input class="flat" type="radio" name="L4" value="Der"></td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L5" value="Izq"> </td>
                            <td>L5</td>
                            <td><input class="flat" type="radio" name="L5" value="Der"></td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
        &nbsp;
        &nbsp;
        {{-- seccion tabla 4  --}}
        <div class="row">
            {{-- <span class="section">Palpacion de Movimiento</span> --}}
            <div class="col">
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Il Cost.</th>
                            <th colspan="2" scope="col">Dors. L</th>
                            <th colspan="2" scope="col">Ser P. Sup</th>
                            <th colspan="2" scope="col">Ser P. Inf</th>
                            <th colspan="2" scope="col">Lig SCMa.</th>
                            <th colspan="2" scope="col">Lig SCMe.</th>
                            <th colspan="2" scope="col">Lig Si</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>


                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="il_cost" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="il_cost" value="Der"> </td>
                            <td> <input class="flat" type="radio" name="dor_l" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="dor_l" value="Der"> </td>
                            <td> <input class="flat" type="radio" name="ser_p_sup" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="ser_p_sup" value="Der"> </td>
                            <td> <input class="flat" type="radio" name="ser_p_inf" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="ser_p_inf" value="Der"> </td>
                            <td> <input class="flat" type="radio" name="SCMa" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="SCMa" value="Der"> </td>
                            <td> <input class="flat" type="radio" name="SCMe" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="SCMe" value="Der"> </td>
                            <td> <input class="flat" type="radio" name="Lig_si" value="Izq"> </td>
                            <td> <input class="flat" type="radio" name="Lig_si" value="Der"> </td>


                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

        &nbsp;
        &nbsp;
        <span class="section">Palpacion de Movimiento</span>
        <div class="row">
            <div class="col-md-4">
                <label for="">Atlas</label>
                <textarea id="message" class="form-control" name="atlas_patr" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"></textarea>
            </div>
            <div class="col-md-4">
                <label for="">Axis</label>
                <textarea id="message" class="form-control" name="axis_patr" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"></textarea>
            </div>
            <div class="col-md-4">
                <label for="">Inferiores</label>
                <textarea id="message" class="form-control" name="inferiores_patr" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;"></textarea>
            </div>

        </div>
        &nbsp;
        &nbsp;

        <div class="">
            <input class="btn btn-primary" type="submit" name="" value="Guardar">
        </div>
    </form>








</section>

@endsection
