@extends('layouts.app')

@section('content')
  <span class="section">Patron de paciente {{($persona->pers_nomb . ' ' . $persona->pers_apel)}} </span>
  <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Nombre
      </th>
      <th class="th-sm">Apellido
      </th>
      <th class="th-sm">DNI
      </th>
      <th class="th-sm">Edad
      </th>
      <th class="th-sm">Domicilio
      </th>
      <th class="th-sm">Telefono
      </th>
      {{-- <th class="th-sm">Ver Planilla
      </th> --}}
      <th class="th-sm">Opciones
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{{$persona->pers_nomb}}</td>
      <td>{{$persona->pers_apel}}</td>
      <td>{{$persona->pers_dni}}</td>
      <td>{{$persona->pers_edad}}</td>
      <td>{{$persona->pers_domi}}</td>
      <td>{{$persona->pers_tele}}</td>
      {{-- <td> <a href="{{route('patron.edit', $persona->id)}}"> <i class="fa fa-eye" title="Ver Planilla"></i> </a> </td> --}}
      <td> <a href="{{route('patron.edit', $persona->id)}}"><i class="fa fa-eye" title="Visualizar Planilla"></i></a> |
         <i class="fa fa-print" title="Generar PDF"></i> |
         <i class="fa fa-remove" title="Eliminar Patron"></i></a> </td>
    </tr>
  </tbody>
</table>
  <a href="{{route('patron.edit', $persona->id)}}"  >Completar Patron</a> |
  <a href="{{route('fichaPersonal.show',$persona->id)}}">Volver a Ficha Personal</a>
@endsection
