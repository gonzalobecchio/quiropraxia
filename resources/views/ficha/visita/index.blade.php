@extends('layouts.app')

@section('content')
<div class="container">
    <section>
        <span class="section">Cuadro Visitas de {{($persona->pers_nomb . ' ' . $persona->pers_apel)}}</span>
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    {{-- <th>#</th> --}}
                    <th class="th-sm">Fecha Creacion
                    </th>
                     <th class="th-sm">Fecha Modificacion
                    </th>
                    {{-- <th class="th-sm">Observacion Adicional - (*) Observacion con contenido
                   </th> --}}

                    <th class="th-sm"> Opciones </th>
                    {{-- <th class="th-sm"> Ver </th> --}}
                    {{-- <th>Id paciente</th> --}}
                    {{-- <th class="th-sm"> Editar </th> --}}
                </tr>
            </thead>
            <tbody>
              {{-- {{dd($visitas)}} --}}
              @foreach ($visitas as $visita)
                <tr>
                    {{-- <td>{{$visita->id}}</td> --}}
                    <td>{{$visita->created_at}}</td>
                    <td>{{$visita->updated_at}}</td>
                    {{-- <td  class="text-center" id="observacionV"> @if (is_null($visita->ObservacionModal))
                    <a id="{{$visita->id}}" href="#!">  Ver observacion </a>
                    @else <a id="{{$visita->id}}" href="#!">  Ver observacion(*) </a> </td>  @endif --}}
                    <td>

                     {{-- <form class="" action="{{route('visita.delete',$visita->id)}}" method="post">
                      @csrf
                      @method('delete') --}}
                      {{-- <input type="hidden" name="persona_id" value="{{$persona_id}}"> --}}
                      {{session(['persona_id' => $persona_id])}}
                      <a href="{{route('visita.edit', [$visita->id,'ver'])}}"><i class="fa fa-eye" title="Visualizar Visita"></i></a>
                    | <a href="{{route('visita.edit', $visita->id)}}"><i class="fa fa-pencil" title="Editar Visita"></i></a>
                    | <a href="{{route('visita.delete',$visita->id)}}" id="deleteVisita"> <i class="fa fa-remove" title="Eliminar Visita"></i> </a>
                    {{-- <button type="submit" class="fa fa-remove"></button>
                    </form> --}}
                      {{--  --}}
                    </td>

                    {{-- <td>{{$visita->persona_id}} </td> --}}
                </tr>

              @endforeach


            </tbody>
        </table>
        &nbsp;
        &nbsp;

        &nbsp;
        &nbsp;

    </section>
    &nbsp;
    &nbsp;
    <a href="{{route('visita.show',$persona_id)}}" class="btn btn-primary btn-sm">Nueva Visita</a>
    <a href="{{route('fichaPersonal.show',$persona_id)}}" class="btn btn-primary btn-sm">Volver a Fichas</a>

</div>



{{-- <div class="modal" tabindex="-1" role="dialog" id="modalObseV">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5  id="titleObse" class="modal-title">Observacion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label for="observacionTabla">Ingrese observacion: (opcional)</label>
        <textarea id="textoModal" class="form-control" name="observacionTabla" rows="8" cols="80"></textarea>
      </div>
      <div class="modal-footer">
          <input type="hidden" id="id_observacion" name="id_observacion" value="">
          <button  id="guardarObservacion" type="button" class="btn btn-primary">Guardar</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div> --}}


@endsection
