@extends('layouts.app')

@section('content')
    <span class="section"> @if ($form == 'ver') Visualizando Visita  @else Editando Visita @endif </span>
<section>

    <form action="{{route('visita.update', $visita->id)}}" method="POST">
        @csrf
        @method('PUT')
        {{-- seccion tabla 1 --}}
        {{-- Esta variable ira por session --}}
        <input type="hidden" name="persona_id" value="{{$visita->persona_id}}">
        <div class="row">
            <div class="col">
                {{-- tabla 1 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Piernas Supino</th>
                            <th colspan="2" scope="col">Piernas Prono</th>
                            <th colspan="2" scope="col">Giro Cab</th>
                            <th colspan="2" scope="col">Pelvis</th>
                            <th colspan="2" scope="col">Rest Sacra</th>
                            <th colspan="2" scope="col">Fosas</th>

                        </tr>
                    </thead>
                    <tbody>

                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>

                        </tr>
                        <tr>
                            <td>
                                <div>
                                    {{-- Izq --}}

                                    <input class="flat" type="checkbox" name="x_supino_izq_1" value="Izq" @if ($visita->x_supino_izq_1 == 'Izq') checked @endif>
                                        <input class="flat" type="checkbox" name="x_supino_izq_2" value="Izq" @if ($visita->x_supino_izq_2 == 'Izq') checked @endif>
                                            <input class="flat" type="checkbox" name="x_supino_izq_3" value="Izq" @if ($visita->x_supino_izq_3 == 'Izq') checked @endif>
                                                <input class="flat" type="checkbox" name="x_supino_izq_4" value="Izq" @if ($visita->x_supino_izq_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat" type="checkbox" name="x_supino_der_1" value="Der" @if ($visita->x_supino_der_1 == 'Izq') checked @endif>
                                        <input class="flat" type="checkbox" name="x_supino_der_2" value="Der" @if ($visita->x_supino_der_2 == 'Izq') checked @endif>
                                            <input class="flat" type="checkbox" name="x_supino_der_3" value="Der" @if ($visita->x_supino_der_3 == 'Izq') checked @endif>
                                                <input class="flat" type="checkbox" name="x_supino_der_4" value="Der" @if ($visita->x_supino_der_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Izq --}}
                                    <input class="flat "type="checkbox" name="x_prono_izq_1" value="Izq" @if ($visita->x_prono_izq_1 == 'Izq') checked @endif>
                                        <input class="flat "type="checkbox" name="x_prono_izq_2" value="Izq" @if ($visita->x_prono_izq_2 == 'Izq') checked @endif>
                                            <input class="flat "type="checkbox" name="x_prono_izq_3" value="Izq" @if ($visita->x_prono_izq_3 == 'Izq') checked @endif>
                                                <input class="flat "type="checkbox" name="x_prono_izq_4" value="Izq" @if ($visita->x_prono_izq_4 == 'Izq') checked @endif>
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{-- Der --}}
                                    <input class="flat "type="checkbox" name="x_prono_der_1" value="Der" @if ($visita->x_prono_der_1 == 'Der') checked @endif>
                                        <input class="flat "type="checkbox" name="x_prono_der_2" value="Der" @if ($visita->x_prono_der_2 == 'Der') checked @endif>
                                            <input class="flat "type="checkbox" name="x_prono_der_3" value="Der" @if ($visita->x_prono_der_3 == 'Der') checked @endif>
                                                <input class="flat "type="checkbox" name="x_prono_der_4" value="Der" @if ($visita->x_prono_der_4 == 'Der') checked @endif>
                                </div>
                            <td><input class="flat "type="radio" class="" id="defaultChecked" name="giro_cab" value="Izq" @if ($visita->giro_cab == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat "type="radio" class="" id="defaultChecked" name="giro_cab" value="Der" @if ($visita->giro_cab == 'Der') checked @endif>
                            </td>
                            <td><input class="flat "type="radio" class="" id="defaultChecked" name="pelvis" value="Izq" @if ($visita->pelvis == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat "type="radio" class="" id="defaultChecked" name="pelvis" value="Der" @if ($visita->pelvis == 'Der') checked @endif>
                            </td>
                            <td><input class="flat "type="radio" class="" id="defaultChecked" name="rest_sacra" value="Izq" @if ($visita->rest_sacra == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat "type="radio" class="" id="defaultChecked" name="rest_sacra" value="Der" @if ($visita->rest_sacra == 'Der') checked @endif>
                            </td>
                            <td> <select class="" name="fosa_izq" value="">
                                    <option value="{{$visita->fosa_izq}}">{{$visita->fosa_izq}}</option>
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>

                            </td>
                            <td> <select class="" name="fosa_der">
                                  <option value="{{$visita->fosa_izq}}">{{$visita->fosa_der}}</option>
                                    <option value="0">0</option>
                                    <option value="0,1">0,1</option>
                                    <option value="0,2">0,2</option>
                                    <option value="0,3">0,3</option>
                                    <option value="0,4">0,4</option>
                                    <option value="0,5">0,5</option>
                                    <option value="0,6">0,6</option>
                                    <option value="0,7">0,7</option>
                                    <option value="0,8">0,8</option>
                                    <option value="0,9">0,9</option>
                                    <option value="1">1</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        &nbsp;
        &nbsp;


        {{-- <span class="section"></span> --}}
        {{-- seccion tabla 2 --}}
        <span class="section">Palpacion Muscular</span>
        <div class="row">
            <div class="col">
                {{-- Tabla 2 --}}
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Angular Omop</th>
                            <th colspan="2" scope="col">Oblicuo Men. Sup</th>
                            <th colspan="2" scope="col">Oblicuo May. Inf</th>
                            <th colspan="2" scope="col">Ecom</th>
                            <th colspan="2" scope="col">Com ></th>
                            <th colspan="2" scope="col">Recto Post > </th>
                            <th colspan="2" scope="col">Recto Post < </th>
                            <th colspan="2" scope="col">Inter Trans</th>
                            <th colspan="2" scope="col">Esplenio cab</th>
                            <th colspan="2" scope="col">Esplenio Cuello</th>
                            <th colspan="2" scope="col">Esc Medio</th>


                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>




                        </tr>
                        <tr>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="angular_omop" value="Izq" @if ($visita->angular_omop == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="angular_omop" value="Der" @if ($visita->angular_omop == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_men" value="Izq" @if ($visita->oblicuo_men == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_men" value="Der" @if ($visita->oblicuo_men == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_may" value="Izq" @if ($visita->oblicuo_may == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="oblicuo_may" value="Der" @if ($visita->oblicuo_may == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="ecom" value="Izq" @if ($visita->ecom == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="ecom" value="Der" @if ($visita->ecom == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="com" value="Izq" @if ($visita->com == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="com" value="Der" @if ($visita->com == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_izq" value="Izq" @if ($visita->recto_post_izq == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_izq" value="Der" @if ($visita->recto_post_izq == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_der" value="Izq" @if ($visita->recto_post_der == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="recto_post_der" value="Der" @if ($visita->recto_post_der == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="inter_trans" value="Izq" @if ($visita->inter_trans == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="inter_trans" value="Der" @if ($visita->inter_trans == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cab" value="Izq" @if ($visita->espelino_cab == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cab" value="Der" @if ($visita->espelino_cab == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cue" value="Izq" @if ($visita->espelino_cue == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="espelino_cue" value="Der" @if ($visita->espelino_cue == 'Der') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="esc_medio" value="Izq" @if ($visita->esc_medio == 'Izq') checked @endif>
                            </td>
                            <td><input class="flat" type="radio" class="" id="defaultChecked" name="esc_medio" value="Der" @if ($visita->esc_medio == 'Der') checked @endif>
                            </td>
                        </tr>

                      </table>

            </div>
        </div>


        &nbsp;
        &nbsp;
        {{-- seccion tabla 3 Tablas vertebras --}}
        <span class="section">Transverso Espinso</span>
        <div class="row">



            {{-- C3-C7 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="C3" value="izq" @if ($visita->C3 == 'Izq') checked @endif> </td>
                            <td>C3</td>
                            <td><input class="flat" type="radio" name="C3" value="Der" @if ($visita->C3 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C4" value="izq" @if ($visita->C4 == 'Izq') checked @endif> </td>
                            <td>C4</td>
                            <td><input class="flat" type="radio" name="C4" value="Der" @if ($visita->C4 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C5" value="izq" @if ($visita->C5 == 'Izq') checked @endif> </td>
                            <td>C5</td>
                            <td><input class="flat" type="radio" name="C5" value="Der" @if ($visita->C5 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C6" value="izq" @if ($visita->C6 == 'Izq') checked @endif> </td>
                            <td>C6</td>
                            <td><input class="flat" type="radio" name="C6" value="Der" @if ($visita->C6 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="C7" value="izq" @if ($visita->C7 == 'Izq') checked @endif> </td>
                            <td>C7</td>
                            <td><input class="flat" type="radio" name="C7" value="Der" @if ($visita->C7 == 'Der') checked @endif>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            {{-- D1-D12 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="D1" value="Izq" @if ($visita->D1 == 'Izq') checked @endif> </td>
                            <td>D1</td>
                            <td><input class="flat" type="radio" name="D1" value="Der" @if ($visita->D1 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D2" value="Izq" @if ($visita->D2 == 'Izq') checked @endif> </td>
                            <td>D2</td>
                            <td><inputclass="flat"  type="radio" name="D2" value="Der" @if ($visita->D2 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D3" value="Izq" @if ($visita->D3 == 'Izq') checked @endif> </td>
                            <td>D3</td>
                            <td><input class="flat" type="radio" name="D3" value="Der" @if ($visita->D3 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D4" value="Izq" @if ($visita->D4 == 'Izq') checked @endif> </td>
                            <td>D4</td>
                            <td><input class="flat" type="radio" name="D4" value="Der" @if ($visita->D4 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D5" value="Izq" @if ($visita->D5 == 'Izq') checked @endif> </td>
                            <td>D5</td>
                            <td><input class="flat" type="radio" name="D5" value="Der" @if ($visita->D5 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D6" value="Izq" @if ($visita->D6 == 'Izq') checked @endif> </td>
                            <td>D6</td>
                            <td><input class="flat" type="radio" name="D6" value="Der" @if ($visita->D6 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D7" value="Izq" @if ($visita->D7 == 'Izq') checked @endif> </td>
                            <td>D7</td>
                            <td><input class="flat" type="radio" name="D7" value="Der" @if ($visita->D7 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D8" value="Izq" @if ($visita->D8 == 'Izq') checked @endif> </td>
                            <td>D8</td>
                            <td><input class="flat" type="radio" name="D8" value="Der" @if ($visita->D8 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D9" value="Izq" @if ($visita->D9 == 'Izq') checked @endif> </td>
                            <td>D9</td>
                            <td><input class="flat" type="radio" name="D9" value="Der" @if ($visita->D9 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D10" value="Izq" @if ($visita->D10 == 'Izq') checked @endif> </td>
                            <td>D10</td>
                            <td><input class="flat" type="radio" name="D10" value="Der" @if ($visita->D10 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D11" value="Izq" @if ($visita->D11 == 'Izq') checked @endif> </td>
                            <td>D11</td>
                            <td><input class="flat" type="radio" name="D11" value="Der" @if ($visita->D11 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="D12" value="Izq" @if ($visita->D12 == 'Izq') checked @endif> </td>
                            <td>D12</td>
                            <td><input class="flat" type="radio" name="D12" value="Der" @if ($visita->D12 == 'Der') checked @endif>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            {{-- L1-L5 --}}
            <div class="col-md-4 col-sm-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Izq</th>
                            <th scope="col">V</th>
                            <th scope="col">Der</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input class="flat" type="radio" name="L1" value="Izq" @if ($visita->L1 == 'Izq') checked @endif> </td>
                            <td>L1</td>
                            <td><input class="flat" type="radio" name="L1" value="Der" @if ($visita->L1 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L2" value="Izq" @if ($visita->L2 == 'Izq') checked @endif> </td>
                            <td>L2</td>
                            <td><input class="flat" type="radio" name="L2" value="Der" @if ($visita->L2 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L3" value="Izq" @if ($visita->L3 == 'Izq') checked @endif> </td>
                            <td>L3</td>
                            <td><input class="flat" type="radio" name="L3" value="Der" @if ($visita->L3 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L4" value="Izq" @if ($visita->L4 == 'Izq') checked @endif> </td>
                            <td>L4</td>
                            <td><input class="flat" type="radio" name="L4" value="Der" @if ($visita->L4 == 'Der') checked @endif>
                            </td>
                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="L5" value="Izq" @if ($visita->L5 == 'Izq') checked @endif> </td>
                            <td>L5</td>
                            <td><input class="flat" type="radio" name="L5" value="Der" @if ($visita->L5 == 'Der') checked @endif>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
        &nbsp;
        &nbsp;
        {{-- seccion tabla 4  --}}
        <div class="row">
            <div class="col">
                <table class="table table-bordered">
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <colgroup span="2"></colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" scope="col">Il Cost.</th>
                            <th colspan="2" scope="col">Dors. L</th>
                            <th colspan="2" scope="col">Ser P. Sup</th>
                            <th colspan="2" scope="col">Ser P. Inf</th>
                            <th colspan="2" scope="col">Lig SCMa.</th>
                            <th colspan="2" scope="col">Lig SCMe.</th>
                            <th colspan="2" scope="col">Lig Si</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>
                            <td>Izq</td>
                            <td>Der</td>


                        </tr>
                        <tr>
                            <td> <input class="flat" type="radio" name="il_cost" value="Izq" @if ($visita->il_cost == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="il_cost" value="Der" @if ($visita->il_cost == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="dor_l" value="Izq" @if ($visita->dor_l == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="dor_l" value="Der" @if ($visita->dor_l == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_sup" value="Izq" @if ($visita->ser_p_sup == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_sup" value="Der" @if ($visita->ser_p_sup == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_inf" value="Izq" @if ($visita->ser_p_inf == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="ser_p_inf" value="Der" @if ($visita->ser_p_inf == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMa" value="Izq" @if ($visita->SCMa == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMa" value="Der" @if ($visita->SCMa == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMe" value="Izq" @if ($visita->SCMe == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="SCMe" value="Der" @if ($visita->SCMe == 'Der') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="Lig_si" value="Izq" @if ($visita->Lig_si == 'Izq') checked @endif> </td>
                            <td> <input class="flat" type="radio" name="Lig_si" value="Der" @if ($visita->Lig_si == 'Der') checked @endif> </td>


                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

        &nbsp;
        &nbsp;
        <span class="section">Palpacion de Movimiento</span>
        <div class="row">

            <div class="col-md-4">
                <label for="">Atlas</label>
                <textarea id="message" class="form-control" name="atlas" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{($visita->atlas)}}</textarea>
            </div>
            <div class="col-md-4">
                <label for="">Axis</label>
                <textarea id="message" class="form-control" name="axis" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{$visita->axis}}</textarea>
            </div>
            <div class="col-md-4">
                <label for="">Inferiores</label>
                <textarea id="message" class="form-control" name="inferiores" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"
                  data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" style="margin-top: 0px; margin-bottom: 0px; height: 107px;">{{$visita->inferiores}}</textarea>
            </div>

        </div>
        &nbsp;
        &nbsp;
        <div class="">
            @if ($form == 'ver')
              <a href="{{route('visita.edit',[$visita->persona_id])}}" class="btn btn-primary btn-sm">Editar</a>
            @else
              <input class="btn btn-primary btn-sm" type="submit" name="" value="Guardar Cambios">
            @endif
            <a href="{{route('visita.index', $visita->persona_id)}}" class="btn btn-primary btn-sm">Volver a Visitas</a>
        </div>
    </form>








</section>

@endsection
