<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}


    <!-- Fonts -->
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}



    {{-- Nuevo --}}
    <!-- Bootstrap -->
    <link href={{asset("styles_template/vendors/bootstrap/dist/css/bootstrap.min.css")}} rel="stylesheet">
    <!-- Font Awesome -->
    <link href={{asset("styles_template/vendors/font-awesome/css/font-awesome.min.css")}} rel="stylesheet">
    <!-- NProgress -->
    {{-- <link href={{asset("styles_template/vendors/nprogress/nprogress.css")}} rel="stylesheet"> --}}
    <!-- iCheck -->
    <link href={{asset("styles_template/vendors/iCheck/skins/flat/green.css")}} rel="stylesheet">

    <!-- bootstrap-progressbar -->
    {{-- <link href={{asset("styles_template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}} rel="stylesheet"> --}}
    <!-- JQVMap -->
    <link href={{asset("styles_template/vendors/jqvmap/dist/jqvmap.min.css" )}} rel="stylesheet" />
    <!-- bootstrap-daterangepicker -->
    {{-- <link href={{asset("styles_template/vendors/bootstrap-daterangepicker/daterangepicker.css")}} rel="stylesheet"> --}}

    <!-- Custom Theme Style -->
    <link href={{asset("styles_template/build/css/custom.min.css")}} rel="stylesheet">
    <!-- MDBootstrap Datatables  -->
    <link href={{asset("styles_template/css/addons/datatables.min.css")}} rel="stylesheet">
    <link href={{asset("styles_template/css/mdb.min.css")}} rel="stylesheet">
    <link href={{asset("styles_template/css/style.css")}} rel="stylesheet">










</head>

<body class="nav-md">

    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{route('home')}}" class="site_title"><i class="fa fa-users"></i> <span>Quiropraxia</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{asset('/img/user.png')}}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <h2>Bienvenido/a, </h2>
                            <span>{{Auth::user()->name}}</span>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{route('home')}}"><i class="fa fa-home"></i>Home<span class="fa fa-chevron-right"></span></a>

                                </li>
                                <li><a><i class="fa fa-male"></i>Pacientes<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        {{-- <li><a href="{{route('nuevoPaciente')}}">Nuevo paciente</a>
                                </li> --}}
                                <li><a href="{{route('listadoPacientes')}}">Lista de pacientes</a></li>

                            </ul>
                            </li>
                            <li><a><i class="fa fa-calendar"></i>Agenda<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    {{-- <li><a href="{{route('agenda.create')}}">Nuevo turno</a>
                            </li> --}}
                            <li><a href="{{route('agenda.create')}}" id="verCalendario">Ver Calendario</a></li>
                            <li><a>Historial de turnos</a></li>
                            </ul>
                            </li>
                            {{-- <li><a href="{{route('fichas.index')}}"><i class="fa fa-archive"></i> Fichas <span class="fa fa-chevron-right"></span></a>
                            </li> --}}
                            <li><a><i class="fa fa-line-chart"></i> Estadisticas <span class="fa fa-chevron-right"></span></a>
                            </li>
                            <li><a><i class="fa fa-save"></i> BackUps <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('backup.btn')}}">Crear BackUp</a></li>
                                </ul>
                            </li>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    {{-- <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>

                        <a data-toggle="tooltip" id="" data-placement="top" title="Logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div> --}}
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <nav class="nav navbar-nav">
                        <ul class=" navbar-right">
                            <li class="nav-item dropdown open" style="padding-left: 15px;">
                                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{{asset('/img/user.png')}}" alt="">{{Auth::user()->name}}
                                </a>
                                <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                    {{-- <a class="dropdown-item" href="javascript:;"> Perfil</a>
                                    <a class="dropdown-item" href="javascript:;">Ayuda</a> --}}
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                 document.getElementById('logout-forms').submit();"><i class="fa fa-sign-out pull-right"></i>Salir</a>
                                    <form id="logout-forms" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                    {{-- <a class="dropdown-item" href="{{Auth::logout()}}"><i class="fa fa-sign-out pull-right"></i>Salir</a> --}}

                                </div>
                            </li>

                            <!-- <li role="presentation" class="nav-item dropdown open">
                                <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                </a>
                                <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                                    <li class="nav-item">
                                        <a class="dropdown-item">
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                                <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item">
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                                <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item">
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                                <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item">
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                                <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <div class="text-center">
                                            <a class="dropdown-item">
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li> -->
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                @yield('content')
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <!-- {{-- <footer> --}}
                {{-- <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div> --}}
                {{-- <div class="clearfix"></div> --}}
            {{-- </footer> --}} -->
            <!-- /footer content -->
        </div>
    </div>





    <script src="{{asset('js/jquery-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/functions.js')}}"></script>


    <!-- jQuery -->
    {{--  <script src="{{asset('styles_template/vendors/jquery/dist/jquery.min.js')}}"></script> --}}
    <!-- Bootstrap -->
    <script src="{{asset('styles_template/vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('styles_template/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('styles_template/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('styles_template/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{asset('styles_template/vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{asset('styles_template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('styles_template/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{asset('styles_template/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('styles_template/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('styles_template/vendors/Flot/jquery.flot.pie.js')}}"></script>

    <script src="{{asset('styles_template/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('styles_template/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('styles_template/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{asset('styles_template/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('styles_template/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('styles_template/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{asset('styles_template/vendors/DateJS/build/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('styles_template/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{asset('styles_template/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('styles_template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('styles_template/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('styles_template/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset("styles_template/build/js/custom.min.js")}}"></script>


    <!-- MDBootstrap Datatables  -->
    <script src="{{asset('js/addons/datatables.min.js')}}"></script>





    <script type="text/javascript">
        $(document).ready(function() {
            $('#dtBasicExample').DataTable({
              "language":{
                "infoEmpty": "Sin registros",
                "zeroRecords": " Sin registros en base de datos.-",
                "lengthMenu": "Listar _MENU_ Registros por pagina",
                "infoFiltered": "(Registro no encontrado de _MAX_ en base de datos)",
                "search":         "Buscar:",
                "info":           "Visualizando _START_ de _END_ de _TOTAL_ registros",
                "paginate": {
                                "first":      "First",
                                "last":       "Last",
                                "next":       "Siguiente",
                                "previous":   "Atras"
                            },
              }
            });
            $('.dataTables_length').addClass('bs-select');

        });
    </script>




    {{-- Estilos de full calendar --}}
    <link href={{asset('fullcalendar/packages/core/main.css')}} rel='stylesheet' />
    <link href={{asset('fullcalendar/packages/daygrid/main.css')}} rel='stylesheet' />
    <link href={{asset('fullcalendar/packages/list/main.css')}} rel='stylesheet' />
    <link href={{asset('fullcalendar/packages/timegrid/main.css')}} rel='stylesheet' />



    {{-- js FullCalendar --}}
    <script src={{asset('fullcalendar/packages/core/main.js')}}></script>
    <script src={{asset('fullcalendar/packages/daygrid/main.js')}}></script>
    <script src={{asset('fullcalendar/packages/list/main.js')}}></script>
    <script src={{asset('fullcalendar/packages/timegrid/main.js')}}></script>
    <script src={{asset('fullcalendar/packages/interaction/main.js')}}></script>


    <script>
        let url = '{{route('agenda.respuesta')}}';

        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ['dayGrid', 'interaction', 'timeGrid', 'list'],
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'today, listWeek, dayGridMonth , timeGridWeek, next',
                },
                buttonText:{
                  today: 'Hoy',
                  list: 'Lista de Turnos',
                  month: 'Turnos por Mes',
                  week: 'Grilla Turnos por Semana',
                },
                noEventsMessage: 'SIN TURNOS EN ESTA SEMANA',

                weekends: false,
                showNonCurrentDates: false,
                eventLimit: true,
                eventLimitText: 'Eventos',
                defaultView: 'dayGridMonth',
                default: "Sin turnos esta semana",

                eventClick: function(info) {
                    //Ejecuta cuando se da click a evento
                    // console.log(info),
                    // console.log(info.event.title);
                    // console.log('Dia ' + info.event.start.getDate()); //dialog
                    // console.log(info.event.start.getMonth() + 1);
                    // console.log('Año ' + info.event.start.getFullYear());
                    // console.log('Horario evento' + hora);
                    // console.log('Minuts evento' + minutos);
                    // console.log(horarioInicio);
                    // console.log(fecha);
                    //console.log(info.event.extendedProps.descripcion);

                    //Manejo de URL en otra pestaña
                    info.jsEvent.preventDefault();
                    if (info.event.url) {
                      window.open(info.event.url);
                    }

                    let dia = info.event.start.getDate() < 10 ? '0' + info.event.start.getDate() : info.event.start.getDate();
                    let mes = info.event.start.getMonth() + 1 < 10 ? '0' + (info.event.start.getMonth() + 1) : info.event.start.getMonth() + 1;
                    let ano = info.event.start.getFullYear();
                    let hora = info.event.start.getHours() < 10 ? '0' + info.event.start.getHours() : info.event.start.getHours();
                    let minutos = info.event.start.getMinutes() < 10 ? '0' + info.event.start.getMinutes() : info.event.start.getMinutes();
                    let horarioInicio = hora + ':' + minutos;
                    let fecha = ano + '-' + mes + '-' + dia;


                    var session = "{{session('zona')}}";
                    //Si descomentamos la condicion se restringe para editar/eliminar evt desde Ver calendario
                    // if (session == 'listado' && session != null && session != "") {
                        if (fecha >= hoy()) {
                            $('#modalEditarEliminar').modal();
                            $('#idEventoEM').val(info.event.id);
                            $('#fechaTurnoEM').val(fecha);
                            $('#horarioInicioEM').val(horarioInicio);
                            $('#fechaTurnoEM').attr('min', hoy());
                            // $('#title_UD').text('Turno: '); modificando
                        }
                    // }


                },
                events: "{{route('agenda.respuesta')}}",


                // events: ,
                //   {
                //   id: '2',
                //   title: 'Esto es un evento',
                //   start:'2020-04-14',
                //   end: '2020-04-14',
                //   descripcion: 'Esto es la descripcion del evento',
                //   textColor: '#FFFFFF',
                //
                // },
                // {
                // id: '4',
                // title: 'Esto es un evento 2',
                // start:'2020-04-16',
                // end: '2020-04-20',
                // descripcion: 'Esto es la descripcion del evento 2',
                // textColor: '#FFFFFF',
                // }
                // ]


            });

            calendar.on('dateClick', function(info) {
                //info que retorna el dia
                // console.log(info);
                var session = "{{session('zona')}}";
                if ($('#horarioInicio').val("") == " ") {
                    alert("horario vacio");
                }
                if (session == 'listado' && session != null && session != "") {
                    let fechaSolicitada = info.dateStr;
                    if (fechaSolicitada >= hoy()) {
                        $('#fechaTurno').val("");
                        $('#horarioInicio').val("");
                        $('#idEvento').val("");
                        $('#fechaTurno').val(info.dateStr); //Recuperando datos desde el click
                        console.log(info.dateStr);
                        var fechaHOY = new Date();
                        console.log(fechaHOY);
                        $('#modal').modal();
                    }
                }

                // calendar.addEvent({title: "Evento 1", date: infor.dateStr});
                //   calendar.rerenderEvents();

            });



            function hoy() {
                let fecha = new Date();
                let dia = fecha.getDate() < 10 ? '0' + fecha.getDate() : fecha.getDate();
                let mes = (fecha.getMonth() + 1) < 10 ? '0' + (fecha.getMonth() + 1) : fecha.getMonth() + 1;
                let ano = fecha.getFullYear();
                let fechaSet = ano + '-' + mes + '-' + dia;
                console.log('Dia de hoy' + fechaSet);
                return fechaSet;
            }





            // var event = calendar.getEventById('2') // an event object!
            // var start = event.start // a property (a Date object)
            // //console.log(event);


            calendar.setOption('locale', 'es');
            calendar.render();



            function limpiarCampos() {
                $('#fechaTurno').val("");
                $('#horarioInicio').val("");
                $('#horarioFin').val("");
                $('#fechaTurnoEM').val("");
                $('#horarioInicioEM').val("");
                $('#idEventoEM').val("");


            }

            //BTN de Modal que pide datos para turno
            $('#agendar').click(function() {
                let ruta = "{{route('agenda.store')}}";
                var obj = addDatos('POST', "modal");
                //console.log(obj); //Llegada de objeto
                $('#fechaTurno').val("");
                $('#horarioInicio').val("");
                $('#idEvento').val("");
                enviarDatos('', obj, ruta);
            });

            //Modal Eliminar y Editar
            $('#eliminar').click(function() {
                //Enviando el id de agendaPersona a eliminar
                let id = $('#idEventoEM').val();
                if (id != null && id != "") {
                    if (confirm('Desea borrar el turno?')) {
                        // var id = $('#idEventoEM').val();
                        let ruta = "{{route('agenda.delete','id')}}".replace("id", id);
                        console.log(ruta);
                        let obj = addDatos("DELETE", "modalEditarEliminar");
                        //console.log(obj); //Llegada de objeto
                        $('#fechaTurnoEM').val("");
                        $('#horarioInicioEM').val("");
                        $('#idEventoEM').val("");
                        enviarDatos('', obj, ruta);
                    }
                }

            });

            $('#editar').click(function() {
                //Enviando el id de agendaPersona a eliminar
                if (confirm('Desea editar turno?')) {
                    var id = $('#idEventoEM').val();
                    let ruta = "{{route('agenda.update','id')}}".replace("id", id);
                    console.log(ruta);
                    var obj = addDatos("PUT", "modalEditarEliminar");
                    //console.log(obj); //Llegada de objeto
                    $('#fechaTurnoEM').val("");
                    $('#horarioInicioEM').val("");
                    $('#idEventoEM').val("");
                    enviarDatos('', obj, ruta);
                }
            });



            //Carga los datos de campos del Modal
            function addDatos(method, modal) {
                if (modal == "modalEditarEliminar") {
                    if ($('#id_personaEM').val() != null && $('#id_personaEM').val() != " " && $('#horarioInicioEM').val() != "") {
                        datos = {
                            //fechaTurno: $('#fechaTurno').val(),
                            start: $('#fechaTurnoEM').val() + ' ' + $('#horarioInicioEM').val(),
                            end: $('#fechaTurnoEM').val() + ' ' + $('#horarioFinEM').val(),
                            backgroundColor: '#8FBC8F',
                            textColor: '#000000',
                            startTime: $('#horarioInicioEM').val(),
                            endTime: $('#horarioInicioEM').val(),
                            persona_id: $('#id_personaEM').val(),
                            '_token': $("meta[name='csrf-token']").attr("content"),
                            '_method': method,
                        }
                    } else {
                        alert('Complete el horario');
                        console.log("ID paciente vacio");

                    }

                }

                if (modal == "modal") {
                    if ($('#id_persona').val() != null && $('#id_persona').val() != " " && $('#horarioInicio').val() != "") {
                        datos = {
                            //fechaTurno: $('#fechaTurno').val(),
                            start: $('#fechaTurno').val() + ' ' + $('#horarioInicio').val(),
                            end: $('#fechaTurno').val() + ' ' + $('#horarioFin').val(),
                            backgroundColor: '#8FBC8F',
                            textColor: '#000000',
                            startTime: $('#horarioInicio').val(),
                            endTime: $('#horarioInicio').val(),
                            persona_id: $('#id_persona').val(),
                            '_token': $("meta[name='csrf-token']").attr("content"),
                            '_method': method,
                        }
                    } else {

                        alert('Complete el horario');
                        console('ID Paciente Vacio');
                    }

                }

                //console.log(datos);
                return datos;
            }

            function enviarDatos(accion, objeto, ruta) {
                //var datos = addDatos('POST')
                $.ajax({
                    type: 'POST',
                    url: ruta + accion,
                    data: objeto,
                    success: function(msg) {
                        $('#modal').modal('hide');
                        console.log(calendar);
                        calendar.refetchEvents();

                    },

                    error: function() {
                        alert('Error');
                    }
                });


            }




        });
    </script>



    <script type="text/javascript">
      var editar = false;
      var ver = false;

        $('#verCalendario').click(function() {
            //alert('Ingresa ok');
            {{session(['zona' => 'menu'])}}
        });



        $('#guardarObservacion').on('click', function () {
          let x_identi = $('#id_observacion').attr('value'); // Id de Visita
          // $('#modalObseV').val('');
          let datosEnvio = { x_identi: x_identi,
                            '_token': $("meta[name='csrf-token']").attr("content"),
                             observacion: $('#textoModal').val(),
                          }
                          $.ajax({
                            type: 'POST',
                            url : "{{route('obserTabla.control')}}",
                            data: datosEnvio,
                            success: function (e) {
                              console.log(e);
                              // $('#textoModal').val(x_identi);
                              $('#modalObseV').modal('hide');
                              console.log('Se guardan Datos ok');
                              location.reload();
                            },
                            error: function (e) {
                              console.log(e);
                            }
                          });
        });


        //Formulario busca datos de observacion en base
       // Consultar a Gabi
       $('#observacionV a').click(function() {
         let x_identi = $(this).attr('id');
         // $('#modalObseV').val('');
         let datosEnvio = {x_identi: x_identi,
                           '_token': $("meta[name='csrf-token']").attr("content"),
                         }
         // $('#textoModal').val(visita_identi);
         $.ajax({
           type: 'POST',
           url : "{{route('searchObservacion.control')}}",
           data: datosEnvio,
           success: function (response) {
             console.log(response);
             console.log('Busqueda de datos ok');
             $('#textoModal').val(response['observacion']);
             $('#id_observacion').attr('value',response['visita_id']);
             $('#titleObse').text('Observacion ' + response['creacion']);
             $('#modalObseV').modal('show');
           },
           error: function (e) {
             console.log(e);
           }
         });

       });






    </script>
  <script src="{{asset('js/fjquery.js')}}"></script>




</body>

</html>
