<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Quiropraxia </title>

    <!-- Bootstrap -->
    <link href={{asset("styles_template/vendors/bootstrap/dist/css/bootstrap.min.css")}} rel="stylesheet">
    <!-- Font Awesome -->
    <link href={{asset("styles_template/vendors/font-awesome/css/font-awesome.min.css" )}} rel="stylesheet">
    <!-- NProgress -->
    <link href={{asset("styles_template/vendors/nprogress/nprogress.css" )}} rel="stylesheet">
    <!-- Animate.css -->
    <link href={{asset("styles_template/vendors/animate.css/animate.min.css" )}} rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href={{asset("styles_template/build/css/custom.min.css" )}} rel="stylesheet">
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form method="post" action="{{route('login')}}">
                        @csrf
                        <h1>Login</h1>
                        <div>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Username" value="{{old('name')}}" required="" autocomplete="name" autofocus />
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        {{-- Recuerdame --}}
                        {{-- <div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div> --}}
                        <div>
                            <div>
                                {{-- <button type="button" class="btn btn-default submit">{{ __('Login') }}</button> --}}
                                  @if (Route::has('password.request'))
                                    <a class="reset_pass" href="#!">Olvidaste tu contraseña?</a>
                                  @endif
                                <input class="btn btn-default  submit" type="submit" name="" value="{{ __('Login') }}">
                            </div>
                            {{-- <a class="btn btn-default submit" href="index.html">Log in</a> --}}
                            <div>

                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Tienes cuenta?
                                <a href="#signup" class="to_register"> Crear Cuenta </a>
                            </p>

                            <div class="clearfix"></div>
                            <br />

                        </div>
                    </form>
                </section>
            </div>
            <div id="register" class="animate form registration_form">
                <section class="login_content">
                    <form method="POST" action="{{route('register')}}">
                      @csrf
                        <h1>Crear Cuenta</h1>
                        <div>
                          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Username" value="{{ old('name') }}" required autocomplete="name" autofocus>

                          @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div>
                          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">

                          @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div>
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">

                          @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required autocomplete="new-password">
                        </div>
                        <div>
                          <button class="btn btn-default " type="submit" name="button">{{ __('Register') }}</button>
                             {{-- <input type="submit" class="btn btn-default submit" name="{{ __('Register') }}" value="{{ __('Register') }}"> --}}
                            {{-- <a class="btn btn-default submit">Registrar</a> --}}
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Eres usuario?
                                <a href="#signin" class="to_register"> Ingresar </a>
                            </p>

                            <div class="clearfix"></div>
                            <br />


                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>


    </div>
    </div>
</body>

</html>
