<?php
  use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



//Pacientes o Personas
Route::get('/pacientes/nuevo', 'PersonaController@showFormPaciente')->name('nuevoPaciente');
Route::get('/pacientes/listado', 'PersonaController@showTablePacientes')->name('listadoPacientes');
Route::post('/home', 'PersonaController@storePersonaPaciente')->name('personas.store');
Route::get('/pacientes/{id}/form/{form?}', 'PersonaController@edit')->name('personas.edit');
Route::put('/pacientes/{id}/update', 'PersonaController@update')->name('personas.update');
//Probando ruta

Route::get('/paciente/delete/{id}', 'PersonaController@delete')->name('persona.delete');
// Route::delete('/paciente/delete/{id}', 'PersonaController@delete')->name('persona.delete');

//Fichas Generales
Route::get('/fichas', 'FichaController@index')->name('fichas.index');
Route::get('/fichas/paciente/{id}', 'FichaController@vistaFichasPersonales')->name('fichaPersonal.show');





//Patron
Route::get('/fichas/patron/paciente/{id}', 'PatronController@index')->name('patron.index');
Route::get('/fichas/patron/nuevo/{id}', 'PatronController@create')->name('patron.create');
Route::post('/fichas/patron/{id}', 'PatronController@storePatron')->name('patron.store');
Route::put('/fichas/patron/update/{id}', 'PatronController@update')->name('patron.update');
Route::get('/fichas/patron/editar/{id}', 'PatronController@edit')->name('patron.edit');



//Visita
Route::get('/fichas/visita/editar/{id}/form/{form?}', 'VisitaController@edit')->name('visita.edit');
Route::put('/fichas/visita/update/{id}', 'VisitaController@update')->name('visita.update');
Route::get('/fichas/visita/nuevaFichaVisita/{id}', 'VisitaController@nuevaFichaVisita')->name('visita.show');
Route::get('fichas/visita/paciente/{id}', 'VisitaController@index')->name('visita.index');
Route::post('/fichas/visita/nuevaVisita/{id}', 'VisitaController@storeVisita')->name('visita.store');


Route::get('fichas/visita/eliminar/{id}', 'VisitaController@delete')->name('visita.delete');



//Control
Route::get('/ficha/control/{id}', 'ControlController@index')->name('control.index');
Route::get('/ficha/control/nuevo/{id}', 'ControlController@nuevoControl')->name('control.create');
Route::post('/ficha/control/store', 'ControlController@store')->name('control.store');
Route::get('/ficha/control/editar/{id}/form/{form?}', 'ControlController@edit')->name('control.edit');
Route::put('/ficha/control/update/{id}', 'ControlController@update')->name('control.update');

Route::get('/ficha/control/delete/{id}', 'ControlController@delete')->name('control.delete');

//Recibe parametro para insertar datos en la tabla
Route::post('/fichas/control/addObservacion', 'ControlController@addObservacion')->name('obserTabla.control');
Route::post('/fichas/control/searchObservacion', 'ControlController@searchObservacion')->name('searchObservacion.control');


//Imagen
Route::get('/ficha/imagen/paciente/{id}', 'ImagenController@index')->name('imagen.index');
Route::post('/ficha/imagen/store', 'ImagenController@store')->name('imagen.store');

//En proceso a futuro
// Route::get('/ficha/imagen/new', 'ImagenController@create')->name('imagen.create');

Route::get('/ficha/imagen/{id}', 'ImagenController@show')->name('imagen.show');
Route::get('/ficha/imagen/delete/{id}', 'ImagenController@delete')->name('imagen.delete');


//Agenda

Route::get('/agenda', 'AgendaController@index')->name('agenda.index');
Route::post('/agenda/store', 'AgendaController@store')->name('agenda.store');

Route::get('/agenda/nuevoTurno/{id?}', 'AgendaController@createNuevoTurno')->name('agenda.create');

Route::get('/agenda/allagendas', 'AgendaController@allagendas')->name('agenda.respuesta');

Route::delete('/agenda/delete/{id}', 'AgendaController@delete')->name('agenda.delete');
Route::put('agenda/{id}/update/', 'AgendaController@update')->name('agenda.update');


//Provincias

Route::get('/provincias/allProvincias', 'ProvinciaController@allProvincias')->name('provincia.allProvincias');
Route::get('/localidad/allLocalidades', 'ProvinciaController@allLocalidades')->name('provincia.allLocalidades');


//En prueba envio de Email ok
Route::get('/envioEmail','EmailController@pruebaEnvios');

//En prueba, backup manual
// Route::get('/backup' ,function () {
//   Artisan::call('backup:run --only-db');
//   return redirect()->route('home');
// })->name('backup.btn');

//Prueba en controlador
Route::get('/backup', 'BackUpController@backupManual')->name('backup.btn');
