<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $guarded = [];

    public function personas()
    {
      return $this->belongsToMany('App\Persona','control_persona')
      ->withTimestamps()
      ->withPivot('user_id');
    }

    public function atlas()
    {
      return $this->belongsToMany('App\Atla','atla_control');
    }

    public function axis()
    {
      return $this->belongsToMany('App\Axis','axis_control');
    }

    public function delineamientos()
    {
      return $this->belongsToMany('App\Delineamiento','delineamiento_control');
    }

    public function pelvis()
    {
      return $this->belongsToMany('App\Pelvis','pelvis_control');
    }

    public function segmentos()
    {
      return $this->belongsToMany('App\Segmento','segmento_control');
    }




}
