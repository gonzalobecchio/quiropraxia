<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
  protected $guarded = [];

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

    public function visitas()
    {
       return $this->hasMany('App\Visita');
    }


    public function patron()
    {
      return $this->hasOne('App\Patron');
    }

}
