<?php

namespace App;

// use App\Paciente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes; //trait "Convierte el atributo delete_at" en instanncia
    //Datatime o Carbon, informa la fecha en que eliminado logicamente.


    public $timestamps = false;

    protected $guarded = [];

    public function paciente()
    {
        return $this->hasOne('App\Paciente');
    }

    public function patron()
    {
        return $this->hasOne('App\Patron');
    }

    public function atlas()
    {
        return $this->belongsToMany('App\Atla', 'atla_patron', 'persona_id', 'atla_id');
    }

    public function axises()
    {
        return $this->belongsToMany('App\Axis', 'axis_patron');
    }

    public function segmentos()
    {
        return $this->belongsToMany('App\Segmento', 'segmento_patron');
    }

    public function delineamientos()
    {
        return $this->belongsToMany('App\Delineamiento', 'delineamiento_patron');
    }

    public function pelvis()
    {
        return $this->belongsToMany('App\Pelvis', 'pelvis_patron');
    }

    public function controls()
    {
        return $this->belongsToMany('App\Control', 'control_persona')
      ->withTimestamps()
      ->withPivot('user_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Imagen');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Localidad');
    }

    public function agendas()
    {
        return $this->belongsToMany('App\Agenda', 'agenda_persona')
      ->using('App\AgendaPersona')
      ->withPivot(['start','end','persona_id', 'agenda_id']);
    }
}
