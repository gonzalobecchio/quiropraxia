<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePersonaPaciente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //Modificando ando
        return [
          // 'nombre' => 'required|alpha',
          'nombre' => 'required|regex:/^[\pL\s\-]+$/u', //Admision de espacios
          'apellido' => 'required|regex:/^[\pL\s\-]+$/u',
          // 'apellido' => 'required|alpha',
          'dni' => 'nullable|unique:personas,pers_dni|digits_between:7,8',
          'domicilio' => 'nullable',
          'cuit' => 'nullable|unique:personas,pers_cuit|numeric',
          'telefono' => 'nullable|numeric',
          'email' => 'nullable|unique:personas,pers_emai|email',
          'nacimiento' => 'nullable|date',
          'esci' => 'nullable',
          'localidad' => 'nullable|regex:/^[\pL\s\-]+$/u',
            //
        ];
    }

    public function attributes()
    {
        return [
        'nombre' => 'Nombre',
        'apellido' => 'Apellido',
        'dni' => 'Dni',
        'domicilio' => 'Domicilio',
        'cuit' => 'CUIT',
        'telefono' => 'Telefono',
        'email' => 'Email',
        'nacimiento' => 'Nacimiento',
        'esci' => 'Estado Civil',
        'localidad' => 'Localidad',
    ];
    }



    public function messages()
    {
        return [
        'nombre.required' => 'Campo :attribute incompleto',
        'nombre.alpha' => 'Campo :attribute solo admite caracteres',
        'apellido.required'  => 'Campo :attribute incompleto',
        'apellido.alpha' => 'Campo :attribute solo admite carateres',
        'dni.required' => 'Campo :attribute incompleto',
        'dni.numeric' => 'Campo :attribute solo admite numeros',
        'dni.unique' => 'El :attribute ya se encuentra registrado',
        'dni.digits_between' => 'El :attribute, debe poseer entre 7 u 8 digitos',
        'domicilio.required'  => 'Campo :attribute incompleto',
        'cuit.required' => 'Campo :attribute incompleto',
        'cuit.numeric' => 'Campo :attribute solo admite numeros',
        'cuit.unique' => 'El :attribute ya se encuentra registrado',
        'telefono.required'  => 'Campo :attribute incompleto',
        'telefono.numeric' => 'Campo :attribute solo admite numeros',
        'email.required' => 'Campo :attribute incompleto',
        'email.unique' => 'El :attribute ya se encuentra registrado',
        'email.email' => 'El formato de :attribute es correo@alguncorreo.com',
        'nacimiento.required'  => 'Campo :attribute incompleto',
        'esci.required'  => 'Campo :attribute incompleto',

    ];
    }
}
