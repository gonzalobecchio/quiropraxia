<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Request;

class UpdatePersonaPaciente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules(Request $request)
     {
         return [
           'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
           'apellido' => 'required|regex:/^[\pL\s\-]+$/u',
           'dni' => 'required|unique:personas,pers_dni,' . $request->id .'|digits_between:7,8',
           'domicilio' => 'required',
           'cuit' => 'required|unique:personas,pers_cuit, '. $request->id .'|numeric',
           'telefono' => 'required|numeric',
           'email' => 'nullable|unique:personas,pers_emai,'. $request->id .'|email',
           'nacimiento' => 'required|date',
           'esci' => 'required',
           'localidad' => 'required|regex:/^[\pL\s\-]+$/u',

             //
         ];
     }

     public function attributes()
     {
         return [
         'nombre' => 'Nombre',
         'apellido' => 'Apellido',
         'dni' => 'Dni',
         'domicilio' => 'Domicilio',
         'cuit' => 'CUIT',
         'telefono' => 'Telefono',
         'email' => 'Email',
         'nacimiento' => 'Nacimiento',
         'esci' => 'Estado Civil',
         'localidad' => 'Localidad',
     ];
     }



     public function messages()
     {
         return [
         'nombre.required' => 'Campo :attribute incompleto',
         'nombre.alpha' => 'Campo :attribute solo admite caracteres',
         'apellido.required'  => 'Campo :attribute incompleto',
         'apellido.alpha' => 'Campo :attribute solo admite carateres',
         'dni.required' => 'Campo :attribute incompleto',
         'dni.numeric' => 'Campo :attribute solo admite numeros',
         'dni.unique' => 'El :attribute ya se encuentra registrado',
         'dni.digits_between' => 'El :attribute, debe poseer entre 7 u 8 digitos',
         'domicilio.required'  => 'Campo :attribute incompleto',
         'cuit.required' => 'Campo :attribute incompleto',
         'cuit.numeric' => 'Campo :attribute solo admite numeros',
         'cuit.unique' => 'El :attribute ya se encuentra registrado',
         'telefono.required'  => 'Campo :attribute incompleto',
         'telefono.numeric' => 'Campo :attribute solo admite numeros',
         'email.unique' => 'El :attribute ya se encuentra registrado',
         'email.email' => 'El formato de :attribute es correo@alguncorreo.com',
         'nacimiento.required'  => 'Campo :attribute incompleto',
         'esci.required'  => 'Campo :attribute incompleto',
         'localidad.required' => 'Campo :attribute incompleto',

     ];
     }
}
