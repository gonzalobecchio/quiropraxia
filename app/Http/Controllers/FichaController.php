<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Persona;
use App\Visita;
use App\Imagen;

class FichaController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      //Tablero con todas las fichas del paciente
      $personas = Persona::all();
      return view('ficha.tableFichasPacientes', ['personas' => $personas]);
    }

    public function vistaFichasPersonales($id)
    {
      //Ficha Personal de cada paciente, Perfil
      $persona = Persona::findOrFail($id);
      $visitas = Visita::all()->where('persona_id',$id)->count();
      $controles = DB::table('control_persona')->where('persona_id', $id)->count();
      $imagenes = Imagen::all()->where('persona_id',$id)->count();
      $datosPerfilPaciente = ['visitas' => $visitas, 'controles' => $controles, 'imagenes' => $imagenes];

      // dd($datosPerfilPaciente['visitas']);
      // $datosTablero  = ['']
      return view('ficha.fichasPersonalesPacientes', ['persona' => $persona, 'datosPerfilPaciente' => $datosPerfilPaciente]);
    }
}
