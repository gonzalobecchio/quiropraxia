<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class BackUpController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function backupManual()
    {
      Artisan::call('backup:run --only-db');
      return redirect()->route('home');

    }
}
