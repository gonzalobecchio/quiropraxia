<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use App\Control;
use App\Atla;
use App\Axis;
use App\Segmento;
use App\Delineamiento;
use App\Pelvis;
use App\Persona;
use App\ControlPersona;

class ControlController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $persona = Persona::find($id);
        $controlPersona = ControlPersona::all()->where('persona_id', $id);
        // $persona = [$persona, $userControl];
        //dd($userControl);


        return view('ficha.control.index', ['controlPersona' => $controlPersona, 'persona' => $persona]);
    }

    public function nuevoControl($id)
    {
        $persona = Persona::find($id);
        $AtAx = Atla::all();
        $Axis = Axis::all();
        $Segmentos = Segmento::all();
        $Delineamientos = Delineamiento::all();
        $Pelvis = Pelvis::all();
        return view('ficha.control.nuevoControl', ['persona' => $persona, 'AtAx' => $AtAx, 'Axis' => $Axis, 'Segmentos' => $Segmentos, 'Delineamiento' => $Delineamientos, 'Pelvis' => $Pelvis]);
    }

    public function edit($id, $form = null)
    {
        $control = Control::findOrFail($id);
        $AtAx = Atla::all();
        $Axis = Axis::all();
        $segmentos = Segmento::all();
        $Delineamientos = Delineamiento::all();
        $Pelvis = Pelvis::all();
        return view('ficha.control.formEditarControl', ['form' => $form , 'id' => $id , 'Delineamientos' => $Delineamientos, 'Pelvis' => $Pelvis , 'Segmentos' => $segmentos , 'AtAx' => $AtAx ,'Axis' => $Axis, 'control' => $control]);
    }

    public function store(Request $request)
    {
        $control = Control::create(['x_supino_izq_1'=> $request->x_supino_izq_1,
                                    'x_supino_izq_2'=> $request->x_supino_izq_2,
                                    'x_supino_izq_3'=> $request->x_supino_izq_3,
                                    'x_supino_izq_4'=> $request->x_supino_izq_4,

                                    'x_supino_der_1'=> $request->x_supino_der_1,
                                    'x_supino_der_2'=> $request->x_supino_der_2,
                                    'x_supino_der_3'=> $request->x_supino_der_3,
                                    'x_supino_der_4'=> $request->x_supino_der_4,

                                    'x_prono_izq_1'=> $request->x_prono_izq_1,
                                    'x_prono_izq_2'=> $request->x_prono_izq_2,
                                    'x_prono_izq_3'=> $request->x_prono_izq_3,
                                    'x_prono_izq_4'=> $request->x_prono_izq_4,

                                    'x_prono_der_1'=> $request->x_prono_der_1,
                                    'x_prono_der_2'=> $request->x_prono_der_2,
                                    'x_prono_der_3'=> $request->x_prono_der_3,
                                    'x_prono_der_4'=> $request->x_prono_der_4,

                                    'giro_cab' => $request->giro_cab,
                                    'pelvis' => $request->pelvis,
                                    'rest_sacra' => $request->rest_sacra,
                                    'fosa_izq'=> $request->fosa_izq,
                                    'fosa_der'=> $request->fosa_der,

                                    'cont_palp' => $request->cont_palp,
                                    'ObservacionControl' => $request->observacionControl,
                                    // 'cont_ajus' => $request->cont_ajus,
                                    // 'cont_pp' => $request->cont_pp,



      ]);


        $persona = Persona::find($request->persona_id); //Trae id de Persona por campo hidden
      $persona->controls()->attach($control->id, ['user_id' => Auth::id()]);  // Adjunta en tabla hija id control y se genera automaticamente id persona
      $control = Control::find($control->id);  // Busca control para poder ingresar a las tablas hijas y sinronizarlas
      $control->atlas()->sync($request->AtAx);
        $control->axis()->sync($request->Axis);
        $control->delineamientos()->sync($request->Delineamiento);
        $control->pelvis()->sync($request->Pelvis);
        $control->segmentos()->sync($request->Segmento);


        return redirect()->route('control.index', $request->persona_id);
    }

    public function update(Request $request, $id)
    {
        $control = Control::updateOrCreate(
            ['id' => $id],
            ['x_supino_izq_1'=> $request->x_supino_izq_1,
                                          'x_supino_izq_2'=> $request->x_supino_izq_2,
                                          'x_supino_izq_3'=> $request->x_supino_izq_3,
                                          'x_supino_izq_4'=> $request->x_supino_izq_4,

                                          'x_supino_der_1'=> $request->x_supino_der_1,
                                          'x_supino_der_2'=> $request->x_supino_der_2,
                                          'x_supino_der_3'=> $request->x_supino_der_3,
                                          'x_supino_der_4'=> $request->x_supino_der_4,

                                          'x_prono_izq_1'=> $request->x_prono_izq_1,
                                          'x_prono_izq_2'=> $request->x_prono_izq_2,
                                          'x_prono_izq_3'=> $request->x_prono_izq_3,
                                          'x_prono_izq_4'=> $request->x_prono_izq_4,

                                          'x_prono_der_1'=> $request->x_prono_der_1,
                                          'x_prono_der_2'=> $request->x_prono_der_2,
                                          'x_prono_der_3'=> $request->x_prono_der_3,
                                          'x_prono_der_4'=> $request->x_prono_der_4,

                                          'giro_cab' => $request->giro_cab,
                                          'pelvis' => $request->pelvis,
                                          'rest_sacra' => $request->rest_sacra,
                                          'fosa_izq'=> $request->fosa_izq,
                                          'fosa_der'=> $request->fosa_der,

                                          'cont_palp' => $request->cont_palp,
                                          'ObservacionControl' => $request->observacionControl,
                                          // 'cont_ajus' => $request->cont_ajus,
                                          // 'cont_pp' => $request->cont_pp,
                                        ]
        );

        $control = Control::find($control->id);  // Busca control para poder ingresar a las tablas hijas y sinronizarlas
        $control->atlas()->sync($request->AtAx);
        $control->axis()->sync($request->Axis);
        $control->delineamientos()->sync($request->Delineamiento);
        $control->pelvis()->sync($request->Pelvis);
        $control->segmentos()->sync($request->Segmento);

        return redirect()->route('control.index', $request->persona_id);
    }


    public function delete(Request $request, $id)
    {
        $control = Control::find($id);
        $control->delete();
        return redirect()->route('control.index', session('persona_id'));
    }


##################################################################################

public function searchObservacion(Request $request)
{

  $datosVisita = $request->except('_token');
  // $visita = Visita::findOrFail($datosVisita['x_identi']);
  $observacion = DB::table('control_persona')->select('observacionAdicional')->where('id',$datosVisita['x_identi'])->value('observacionAdicional');
  $creacion = DB::table('control_persona')->select('created_at')->where('id',$datosVisita['x_identi'])->value('created_at');
  $datosVuelta = array('observacion' => $observacion,
                       'creacion' => $creacion,
                       'visita_id' => $datosVisita['x_identi']);
  // print_r($observacion);
    return ($datosVuelta);
  // return $observacion;
}

//Agrega observacion
public function addObservacion(Request $request)
{
  print_r($request->all());
  ControlPersona::updateOrCreate(['id' => $request['x_identi']],
                         ['observacionAdicional' => $request['observacion']]);
  // print_r($request->all());
}



}
