<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;



use App\Http\Requests\StorePersonaPaciente;
use App\Http\Requests\UpdatePersonaPaciente;
use App\Persona;
use App\Paciente;
use App\Provincia;
use App\Localidad;

class PersonaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function showFormPaciente()
    {
        $provincias = Provincia::orderBy('prov_nomb', 'asc')->get();
        $localidades = Localidad::orderBy('loca_nomb','asc')->get();


        return view('paciente.formNuevoPaciente', ['provincias' => $provincias, 'localidades' => $localidades]);
    }

    public function showTablePacientes()
    {
        $personas = Persona::orderBy('pers_apel', 'asc')->get();
        return view('paciente.tablePacientes', ['personas' => $personas]);
    }

    public function calcularTiempo($fechaInicio, $fechaFin)
    {
        //indice 0 = años
        //indice 1= meses
        //indice 2 = dias
        //indice 11 = total en dias
        $datetime1 = date_create($fechaInicio);
        $datetime2 = date_create($fechaFin);
        $interval = date_diff($datetime1, $datetime2);

        $tiempo=array();

        foreach ($interval as $valor) {
            $tiempo[]=$valor;
        }

        return $tiempo;
    }


    public function storePersonaPaciente(StorePersonaPaciente $request)
    {
        // ------------------
          // dd($request->file);
          //
          // $archivo = $request->file('file');
          // $nombreImag = $archivo->getClientOriginalName();
          // $archivo->move('images', $nombreImag);
        // ------------------------------
        $validated = $request->validated(); //Reglas de validacion
        $personas = Persona::create([
                                 'pers_nomb' => Str::title($request->nombre),
                                 'pers_apel' => Str::title($request->apellido),
                                 'pers_dni' => $request->dni,
                                 'pers_domi' => Str::title($request->domicilio),
                                 'pers_tele' => $request->telefono,
                                 'pers_emai' => $request->email,
                                 'pers_cuit' => $request->cuit,
                                 'pers_naci' => $request->nacimiento,
                                 'pers_edad' => $this->calcularTiempo($request->nacimiento, date('Y-m-d'))[0],
                                 'pers_cahi' => $request->cahi,
                                 'pers_ocup' => Str::title($request->ocupacion),
                                 'pers_esci' => $request->esci,
                                 'loca_id' => Str::title($request->localidad),
                                 'provincia_id' => $request->provincia,
                                 'user_id' => Auth::id(),
                                ]);

        $personas->paciente()->create(['paci_reco' => Str::title($request->recomendado),
                                      'paci_ciru' => $request->cirujias,
                                      'paci_frac' => $request->fracturas,
                                      'paci_medi' => $request->medicamentos,
                                      'paci_obs1' => $request->obs1,
                                      'paci_obs2' => $request->obs2,
                                      'paci_obs3' => $request->obs3,]);
        $personas->patron()->create([]);


        $personas->save();

        return redirect()->route('listadoPacientes');
    }


    public function edit($id, $form = null)
    {
        $id_provincia;
        $persona = Persona::findOrFail($id);
        $provincias = Provincia::all();
        // $localidades = Localidad::all();
        //Verificar luego, porque esta mal de esta manera
        // $localidad = Localidad::findOrFail($persona->loca_id);
        if (is_null($persona->provincia_id)) {
          $id_provincia = '14';
          //Provisorio por cambio estructura base datos
        }else{
          $id_provincia = $persona->provincia_id;
        }

        $prov = Provincia::findOrFail($id_provincia);




        return view('paciente.formEditarPaciente', ['prov' => $prov, 'form' => $form ,'persona' => $persona, 'provincias' => $provincias,]);
    }

    public function update(UpdatePersonaPaciente $request, $id)
    {
        $validated = $request->validated();
        Persona::where('id', $id)
                ->update(['pers_nomb' => Str::title($request->nombre),
                          'pers_apel' => Str::title($request->apellido),
                          'pers_dni' => $request->dni,
                          'pers_domi' => $request->domicilio,
                          'pers_tele' => $request->telefono,
                          'pers_emai' => $request->email,
                          'pers_cuit' => $request->cuit,
                          'pers_naci' => $request->nacimiento,
                          'pers_edad' => $this->calcularTiempo($request->nacimiento, date('Y-m-d'))[0],
                          'pers_cahi' => $request->cahi,
                          'pers_ocup' => Str::title($request->ocupacion),
                          'pers_esci' => $request->esci,
                          'loca_id' => $request->localidad, //Ahora es texto
                          'provincia_id' => $request->provincia,
                          'user_id' => Auth::id(),
       ]);

        Paciente::where('persona_id', $id)
                ->update(['paci_reco' =>$request->recomendado,
                          'paci_ciru' => $request->cirujias,
                          'paci_frac' => $request->fracturas,
                          'paci_medi' => $request->medicamentos,
                          'paci_obs1' => $request->obs1,
                          'paci_obs2' => $request->obs2,
                          'paci_obs3' => $request->obs3,
              ]);

        return redirect()->route('listadoPacientes');

        // Verificar formas mas eficientes para guardar relaciones
      // $persona = Persona::find($id);
      // $persona->pers_nomb = Str::title($request->nombre);
      // $persona->pers_apel = Str::title($request->apellido);
      // $persona->pers_dni = $request->dni;
      // $persona->pers_domi = $request->domicilio;
      // $persona->pers_tele = $request->telefono;
      // $persona->pers_emai = $request->email;
      // $persona->pers_cuit = $request->cuit;
      // $persona->pers_naci = $request->nacimiento;
      // $persona->pers_edad = $request->edad;
      // $persona->pers_cahi = $request->cahi;
      // $persona->pers_ocup = Str::title($request->ocupacion);
      // $persona->pers_esci = Str::title($request->esci);
      // $persona->user_id = Auth::id();
      //
      // $persona->paciente->paci_reco = $request->recomendado;
      // $persona->paciente->paci_ciru = $request->cirujias;
      // $persona->paciente->paci_frac = $request->fracturas;
      // $persona->paciente->paci_medi = $request->medicamentos;
      // $persona->paciente->paci_obs1 = $request->obs1;
      // $persona->paciente->paci_obs2 = $request->obs2;
      // $persona->paciente->paci_obs3 = $request->obs3;
      // dd($persona);
      // dd($persona->save());
    }

    public function delete($id)
    {
        $persona = Persona::findOrFail($id);
        $persona->delete();
        return redirect()->route('listadoPacientes');
    }
}
