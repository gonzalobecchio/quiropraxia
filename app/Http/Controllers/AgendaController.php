<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;



use App\Agenda;
use App\Persona;
use App\AgendaPersona;

class AgendaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('agenda.index');
    }

    // public function createNuevoTurno($id)
    // {
    //     Session::put('zona', 'listadoPaciente');
    //     //Obtener el id de paciente por variable de session t pasarlo
    //     return view('agenda.nuevoTurno', ['id' => $id]);
    // }

    public function createNuevoTurno($id = null)
    {
        //Obtener el id de paciente por variable de session t pasarlo
        // $id = Session::get('persona_id', null);

        // $persona  = Persona::findOrFail($id);
        if ($id != null) {
            session(['zona' => 'listado']);
           $persona  = Persona::findOrFail($id);
        }else{
          $persona = null;
        }

        return view('agenda.nuevoTurno', ['id' => $id, 'persona' => $persona]);
    }

    public function existePaciente($id)
    {
      return Persona::where('id', $id)->exists();
    }

    public function existeAgenda($id)
    {
      return Agenda::where('id',$id)->exists();
    }

    public function store(Request $request)
    {
        //Validar Datos sino existe paciente
        $datos  = $request->except('_token', '_method');
        if ($this->existePaciente($datos['persona_id'])) {
          //var_dump($datos['persona_id']);
          $agenda = Agenda::create([]); //Creacion de Agenda
          $persona = Persona::find($datos['persona_id']);
          $persona->agendas()
          ->attach($agenda->id, ['endTime' => $datos['endTime'] ,'startTime' =>$datos['startTime'] ,'start' => $datos['start'], 'end' => $datos['end'],'backgroundColor' => $datos['backgroundColor'], 'textColor' => $datos['textColor']]);
        }

    }


    public function setDatosAdicionales($json)
    {
        //Llegada de datos en json y decodificacion a Array
        $array = json_decode($json, true);
        // data_fill($array[0], 'title', 'Gonzalo');
        // dd($array);
        // foreach ($array as $key) {
        //     $nombre = DB::table('personas')->select('pers_nomb')->where('id', $array[$key]['persona_id'])->value('pers_nomb');
        //     data_fill($array[$key], 'title', $nombre);
        // }
        for ($i=0; $i <  count($array); $i++) {
            $apellido = DB::table('personas')->select('pers_apel')->where('id', $array[$i]['persona_id'])->value('pers_nomb');
            $nombre = DB::table('personas')->select('pers_nomb')->where('id', $array[$i]['persona_id'])->value('pers_nomb');
            $nombreCompleto = $apellido .' '. $nombre;
            data_fill($array[$i], 'title', $nombreCompleto); //Agregamos title para nombre en turno
            data_fill($array[$i], 'allDay', false);
            data_fill($array[$i], 'url', route('fichaPersonal.show',$array[$i]['persona_id']));

        }
        return $array;
    }


    public function delete($id)
    {
        //dd($request->all());
          if ($this->existeAgenda(e($id))) {
            $agendaPersona = AgendaPersona::findOrFail($id);
            $agenda = Agenda::find($agendaPersona->agenda_id);
            $agendaPersona->delete();
            $agenda->delete();
          }


        }




    public function update(Request $request, $id)
    {
      $agendaPersona = DB::table('agenda_persona')->where('id', $id)->update(['start'=> $request->start]);

        //Verificacion de actulizacon con metodo de Eloquent
        // dd($request->start);
        // $datos = $request->all();
        // echo $datos['start'];
        // echo $id;
        // $persona = Persona::findOrFail($request->persona_id);
        // dd($persona->agendas());
        // $persona->agendas()->updateExistingPivot($id, ['start' => $datos['start']]);
    }

    public function allagendas(Request $request)
    {
        $allAgendas = AgendaPersona::all();
        // $json = response()->json($allAgendas->toJson());
        $json = $allAgendas->toJson();
        //$this->setDatosAdicionales($json);
        return response()->json($this->setDatosAdicionales($json));
    }
}
