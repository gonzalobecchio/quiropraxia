<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;


// use App\Http\Requests\StoreImagen;

use App\Imagen;
use App\Persona;

class ImagenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $pxWhidth = 782;  //Ancho
    private $pxHeight = 700; //Altura

    public function index($id)
    {
        $persona = Persona::findOrFail($id);
        $imagens = $persona->imagens;
        return view('ficha.imagen.index', ['persona' => $persona, 'imagens' => $imagens]);
    }

    public function create()
    {
        return view('ficha.imagen.formNuevaImagen');
    }


    public function existeImgDB($originalName)
    {
        return Imagen::where('imag_nombre', $originalName)->exists();
    }

    public function existeImgDisk($originalName)
    {
        return Storage::disk('public')->exists($originalName);
    }


    public function tratarImagen($photo)
    {
        //Obtenemos la imagen para el tratamiento
        $url = Str::after(Storage::url($photo->getClientOriginalName()), '/');
        $img = Image::make($url);
        $img = $img->resize($this->pxWhidth, $this->pxHeight, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img = $img->save($url, 72); //dpi el peso de imagen
        $nuevaphoto = $img;
        return $nuevaphoto;
    }


    public function store(Request $request)
    {
        if ($request->hasFile('file')) { //Consulta si viene archivo
            $photo = $request->file('file');
            if ($this->existeImgDB($photo->getClientOriginalName()) && $this->existeImgDisk($photo->getClientOriginalName())) {
                //Ingresa si el arhivo ya existe fisicamente
                //Ingresa si el archivo existe en base de datos
                // dd('Archivo existente');
                $persona = Persona::findOrFail($request->persona_id);
                $imagens = $persona->imagens;
                return view('ficha.imagen.index', ['persona' => $persona, 'imagens' => $imagens]);
            } else {
                Storage::disk('public')->put($photo->getClientOriginalName(), \File::get($photo));
                // $img  = Image::make('storage/paciente_2.jpeg');
                // $url =  Str::after(Storage::url($photos->getClientOriginalName()),'/');
                // $img = $img->resize(300,300);
                // dd($url);
                $newPhoto = $this->tratarImagen($photo);
                //dd($newPhoto->filesize());
                $imagen = Imagen::create(['imag_ruta' => Storage::url($photo->getClientOriginalName()),
                                  'imag_nombre' => $photo->getClientOriginalName(),
                                  'imag_size' => $newPhoto->filesize(),//Ingresan los datos del archivo en Byte
                                  'imag_exte' => $photo->extension(),
                                  'imag_obse' => $request->observacion,
                                  'persona_id'=> $request->persona_id]);
                return redirect()->route('imagen.index', $request->persona_id);
            }
        } else {
            return redirect()->route('imagen.index', $request->persona_id);
        }
    }


    public function show($id)
    {
        $imagen = Imagen::findOrFail($id);
        return view('ficha.imagen.visor', ['imagen' => $imagen]);
    }

    public function eliminarArchivoDirectorio($path)
    {
        if (\File::exists(public_path($path))) {
            \File::delete(public_path($path));
        } else {
            dd('Archivo no existe');
        }
    }

    public function delete($id)
    {
        $imagen = Imagen::findOrFail($id);
        $this->eliminarArchivoDirectorio($imagen->imag_ruta);
        //$path = $imagen->imag_ruta;
        $imagen->delete(); //Elimina registro de base de datos
        return redirect()->route('imagen.index', $imagen->persona_id);
    }
}
