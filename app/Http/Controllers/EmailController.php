<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Emails;


class EmailController extends Controller
{
    public function pruebaEnvios()
    {
      Mail::to('juliiabonetto@gmail.com')->send(new Emails());
    }
}
