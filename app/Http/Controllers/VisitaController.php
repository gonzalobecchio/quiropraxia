<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visita;
use App\Persona;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class VisitaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $visitas = Visita::all()->where('persona_id', $id);
        // dd($visitas);

        $persona = Persona::find($id);

        // dd($nombrePaciente->pers_nomb);
        // (['pers_nomb' => Persona::find($id)->value('pers_nomb'),
        //                    'pers_apel' => Persona::find($id)->value('pers_apel')]);
        // dd($datosPaciente);
        // dd($paciente);


        return view('ficha.visita.index', ['persona_id' => $id, 'visitas' => $visitas, 'persona' => $persona]);
    }

    public function nuevaFichaVisita($id)
    {
        return view('ficha.visita.nuevaFichaVisita', ['id'=> $id]);
    }

    public function edit($id, $form = null)
    {
        $visita = Visita::findOrFail($id);
        return view('ficha.visita.formEditarVisita', ['form' => $form ,'visita' => $visita]);
    }

    public function update(Request $request, $id)
    {
        $visita = Visita::updateOrCreate(
            ['id' => $id],
            ['x_supino_izq_1'=> $request->x_supino_izq_1,
                               'x_supino_izq_2'=> $request->x_supino_izq_2,
                               'x_supino_izq_3'=> $request->x_supino_izq_3,
                               'x_supino_izq_4'=> $request->x_supino_izq_4,

                               'x_supino_der_1'=> $request->x_supino_der_1,
                               'x_supino_der_2'=> $request->x_supino_der_2,
                               'x_supino_der_3'=> $request->x_supino_der_3,
                               'x_supino_der_4'=> $request->x_supino_der_4,

                               'x_prono_izq_1'=> $request->x_prono_izq_1,
                               'x_prono_izq_2'=> $request->x_prono_izq_2,
                               'x_prono_izq_3'=> $request->x_prono_izq_3,
                               'x_prono_izq_4'=> $request->x_prono_izq_4,

                               'x_prono_der_1'=> $request->x_prono_der_1,
                               'x_prono_der_2'=> $request->x_prono_der_2,
                               'x_prono_der_3'=> $request->x_prono_der_3,
                               'x_prono_der_4'=> $request->x_prono_der_4,

                               'giro_cab' => $request->giro_cab,
                               'pelvis' => $request->pelvis,
                               'rest_sacra' => $request->rest_sacra,
                               'fosa_izq'=> $request->fosa_izq,
                               'fosa_der'=> $request->fosa_der,
                               'angular_omop' => $request->angular_omop,
                               'oblicuo_men' => $request->oblicuo_men,
                               'oblicuo_may' => $request->oblicuo_may,
                               'ecom' => $request->ecom,
                               'com' => $request->com,
                               'recto_post_izq' =>$request->recto_post_izq,
                               'recto_post_der' =>$request->recto_post_der,
                               'inter_trans' =>$request->inter_trans,
                               'espelino_cab' => $request->espelino_cab,
                               'espelino_cue' => $request->espelino_cue,
                               'esc_medio' => $request->esc_medio,
                               'C3' => $request->C3,
                               'C4' => $request->C4,
                               'C5' => $request->C5,
                               'C6' => $request->C6,
                               'C7' => $request->C7,
                               'D1' => $request->D1,
                               'D2' => $request->D2,
                               'D3' => $request->D3,
                               'D4' => $request->D4,
                               'D5' => $request->D5,
                               'D6' => $request->D6,
                               'D7' => $request->D7,
                               'D8' => $request->D8,
                               'D9' => $request->D9,
                               'D10' => $request->D10,
                               'D11' => $request->D11,
                               'D12' => $request->D12,
                               'L1' => $request->L1,
                               'L2' => $request->L2,
                               'L3' => $request->L3,
                               'L4' => $request->L4,
                               'L5' => $request->L5,

                               'il_cost' => $request->il_cost,
                                'dor_l' => $request->dor_l,
                                'ser_p_sup' => $request->ser_p_sup,
                                'ser_p_inf' => $request->ser_p_inf,
                                'SCMa' => $request->SCMa,
                                'SCMe' => $request->SCMe,
                                'Lig_si' => $request->Lig_si,
                                'atlas'=> $request->atlas,
                                'axis' => $request->axis,
                                'inferiores' => $request->inferiores,

                                'visi_fech' => today(),
                               'persona_id' => $request->persona_id,

    ]
        );
        return redirect()->route('visita.index', $request->persona_id);
    }


    public function storeVisita(Request $request, $id)
    {
        // dd($request->file('file'));


        $visita  = Visita::create(['x_supino_izq_1'=> $request->x_supino_izq_1,
                                 'x_supino_izq_2'=> $request->x_supino_izq_2,
                                 'x_supino_izq_3'=> $request->x_supino_izq_3,
                                 'x_supino_izq_4'=> $request->x_supino_izq_4,

                                 'x_supino_der_1'=> $request->x_supino_der_1,
                                 'x_supino_der_2'=> $request->x_supino_der_2,
                                 'x_supino_der_3'=> $request->x_supino_der_3,
                                 'x_supino_der_4'=> $request->x_supino_der_4,

                                 'x_prono_izq_1'=> $request->x_prono_izq_1,
                                 'x_prono_izq_2'=> $request->x_prono_izq_2,
                                 'x_prono_izq_3'=> $request->x_prono_izq_3,
                                 'x_prono_izq_4'=> $request->x_prono_izq_4,

                                 'x_prono_der_1'=> $request->x_prono_der_1,
                                 'x_prono_der_2'=> $request->x_prono_der_2,
                                 'x_prono_der_3'=> $request->x_prono_der_3,
                                 'x_prono_der_4'=> $request->x_prono_der_4,

                                 'giro_cab' => $request->giro_cab,
                                 'pelvis' => $request->pelvis,
                                 'rest_sacra' => $request->rest_sacra,
                                 'fosa_izq'=> $request->fosa_izq,
                                 'fosa_der'=> $request->fosa_der,
                                 'angular_omop' => $request->angular_omop,
                                 'oblicuo_men' => $request->oblicuo_men,
                                 'oblicuo_may' => $request->oblicuo_may,
                                 'ecom' => $request->ecom,
                                 'com' => $request->com,
                                 'recto_post_izq' =>$request->recto_post_izq,
                                 'recto_post_der' =>$request->recto_post_der,
                                 'inter_trans' =>$request->inter_trans,
                                 'espelino_cab' => $request->espelino_cab,
                                 'espelino_cue' => $request->espelino_cue,
                                 'esc_medio' => $request->esc_medio,
                                 'C3' => $request->C3,
                                 'C4' => $request->C4,
                                 'C5' => $request->C5,
                                 'C6' => $request->C6,
                                 'C7' => $request->C7,
                                 'D1' => $request->D1,
                                 'D2' => $request->D2,
                                 'D3' => $request->D3,
                                 'D4' => $request->D4,
                                 'D5' => $request->D5,
                                 'D6' => $request->D6,
                                 'D7' => $request->D7,
                                 'D8' => $request->D8,
                                 'D9' => $request->D9,
                                 'D10' => $request->D10,
                                 'D11' => $request->D11,
                                 'D12' => $request->D12,
                                 'L1' => $request->L1,
                                 'L2' => $request->L2,
                                 'L3' => $request->L3,
                                 'L4' => $request->L4,
                                 'L5' => $request->L5,

                                 'il_cost' => $request->il_cost,
                                  'dor_l' => $request->dor_l,
                                  'ser_p_sup' => $request->ser_p_sup,
                                  'ser_p_inf' => $request->ser_p_inf,
                                  'SCMa' => $request->SCMa,
                                  'SCMe' => $request->SCMe,
                                  'Lig_si' => $request->Lig_si,
                                  'atlas'=> $request->atlas,
                                  'axis' => $request->axis,
                                  'inferiores' => $request->inferiores,

                                  'visi_fech' => today(),
                                  'persona_id' => $id,


      ]);
        return redirect()->route('visita.index', $id);
    }

    public function delete(Request $request, $id)
    {
        $visita = Visita::findOrFail($id);
        $visita->delete();
        return redirect()->route('visita.index', session('persona_id'));
        // return redirect()->route('visita.index',$request->persona_id);
    }


    //Busca Observacion para mostrar
    // public function searchObservacion(Request $request)
    // {
    //
    //   $datosVisita = $request->except('_token');
    //   // $visita = Visita::findOrFail($datosVisita['x_identi']);
    //   $observacion = DB::table('visitas')->select('ObservacionModal')->where('id',$datosVisita['x_identi'])->value('ObservacionModal');
    //   $creacion = DB::table('visitas')->select('created_at')->where('id',$datosVisita['x_identi'])->value('created_at');
    //   $datosVuelta = array('observacion' => $observacion,
    //                        'creacion' => $creacion,
    //                        'visita_id' => $datosVisita['x_identi']);
    //   // print_r($observacion);
    //     return ($datosVuelta);
    //   // return $observacion;
    // }
    //
    // //Agrega observacion
    // public function addObservacion(Request $request)
    // {
    //   print_r($request->all());
    //   Visita::updateOrCreate(['id' => $request['x_identi']],
    //                          ['ObservacionModal' => $request['observacion']]);
    //   // print_r($request->all());
    // }
}
