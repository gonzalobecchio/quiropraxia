<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patron;
use App\Atla;
use App\Axis;
use App\Persona;
use App\Segmento;
use App\Delineamiento;
use App\Pelvis;

class PatronController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $persona = Persona::findOrFail($id);
        return view('ficha.patron.index', ['id' => $id,'persona' => $persona]);
    }

    public function create($id)
    {
        $AtAx = Atla::all();
        $Axis = Axis::all();
        $Segmentos = Segmento::all();
        $Delineamientos = Delineamiento::all();
        $Pelvis = Pelvis::all();

        //Obtenemos todos los Atlas
        return view('ficha.patron.formNuevoPatron', ['id' => $id , 'Pelvis' => $Pelvis, 'Delineamiento' => $Delineamientos ,'AtAx' => $AtAx, 'Axis' => $Axis, 'Segmentos' => $Segmentos]);
    }



    public function edit($id)
    {
        
        $patron = Patron::findOrFail($id);
        $axis = Axis::all();
        $atlas = Atla::all();
        $segmentos = Segmento::all();
        $Delineamientos = Delineamiento::all();
        $Pelvis = Pelvis::all();
        return view('ficha.patron.formEditPatron', ['Delineamientos' => $Delineamientos, 'Pelvis' => $Pelvis , 'Segmentos' => $segmentos , 'Atlas'=>$atlas ,'Axis'=>$axis , 'patron' => $patron, 'id'=> $id]);
    }

    public function update(Request $request, $id) //id corresondiente a la persona de patron
    {
        // $patron = Patron::firstOrCreate(['persona_id' => $id])
        // dd($id);
        $patron = Patron::updateOrCreate(
            ['persona_id' => $id],
            ['x_supino_izq_1'=> $request->x_supino_izq_1,
                               'x_supino_izq_2'=> $request->x_supino_izq_2,
                               'x_supino_izq_3'=> $request->x_supino_izq_3,
                               'x_supino_izq_4'=> $request->x_supino_izq_4,

                               'x_supino_der_1'=> $request->x_supino_der_1,
                               'x_supino_der_2'=> $request->x_supino_der_2,
                               'x_supino_der_3'=> $request->x_supino_der_3,
                               'x_supino_der_4'=> $request->x_supino_der_4,

                               'x_prono_izq_1'=> $request->x_prono_izq_1,
                               'x_prono_izq_2'=> $request->x_prono_izq_2,
                               'x_prono_izq_3'=> $request->x_prono_izq_3,
                               'x_prono_izq_4'=> $request->x_prono_izq_4,

                               'x_prono_der_1'=> $request->x_prono_der_1,
                               'x_prono_der_2'=> $request->x_prono_der_2,
                               'x_prono_der_3'=> $request->x_prono_der_3,
                               'x_prono_der_4'=> $request->x_prono_der_4,

                               'giro_cab' => $request->giro_cab,
                               'pelvis' => $request->pelvis,
                               'rest_sacra' => $request->rest_sacra,
                               'fosa_izq'=> $request->fosa_izq,
                               'fosa_der'=> $request->fosa_der,
                               'angular_omop' => $request->angular_omop,
                               'oblicuo_men' => $request->oblicuo_men,
                               'oblicuo_may' => $request->oblicuo_may,
                               'ecom' => $request->ecom,
                               'com' => $request->com,
                               'recto_post_izq' =>$request->recto_post_izq,
                               'recto_post_der' =>$request->recto_post_der,
                               'inter_trans' =>$request->inter_trans,
                               'espelino_cab' => $request->espelino_cab,
                               'espelino_cue' => $request->espelino_cue,
                               'esc_medio' => $request->esc_medio,
                               'C3' => $request->C3,
                               'C4' => $request->C4,
                               'C5' => $request->C5,
                               'C6' => $request->C6,
                               'C7' => $request->C7,
                               'D1' => $request->D1,
                               'D2' => $request->D2,
                               'D3' => $request->D3,
                               'D4' => $request->D4,
                               'D5' => $request->D5,
                               'D6' => $request->D6,
                               'D7' => $request->D7,
                               'D8' => $request->D8,
                               'D9' => $request->D9,
                               'D10' => $request->D10,
                               'D11' => $request->D11,
                               'D12' => $request->D12,
                               'L1' => $request->L1,
                               'L2' => $request->L2,
                               'L3' => $request->L3,
                               'L4' => $request->L4,
                               'L5' => $request->L5,

                               'il_cost' => $request->il_cost,
                                'dor_l' => $request->dor_l,
                                'ser_p_sup' => $request->ser_p_sup,
                                'ser_p_inf' => $request->ser_p_inf,
                                'SCMa' => $request->SCMa,
                                'SCMe' => $request->SCMe,
                                'Lig_si' => $request->Lig_si,
                                'atlas_patr'=> $request->atlas_patr,
                                'axis_patr' => $request->axis_patr,
                                'inferiores_patr' => $request->inferiores_patr,
                                // 'persona_id' => $id,
                              ]
        );
        $persona = Persona::find($id);
        // Sincronizacion de datos Inserta
        //y actualiza datos de multi select
        $persona->atlas()->sync($request->AtAx);
        $persona->axises()->sync($request->Axis);
        $persona->segmentos()->sync($request->Segmento);
        $persona->delineamientos()->sync($request->Delineamiento);
        $persona->pelvis()->sync($request->Pelvis);


        return redirect()->route('fichaPersonal.show', $request->persona_id);

        // return redirect()->route('patron.index',$request->persona_id);

    // dd($request->AtAx);

 // $atlasSeleccionados =
    }

    public function storePatron(Request $request, $id) //id corresondiente a la persona de patron
    {

        // $patron = Patron::firstOrCreate(['persona_id' => $id])
        // dd($id);
        $patron = Patron::updateOrCreate(
            ['persona_id' => $id],
            ['x_supino_izq_1'=> $request->x_supino_izq_1,
                               'x_supino_izq_2'=> $request->x_supino_izq_2,
                               'x_supino_izq_3'=> $request->x_supino_izq_3,
                               'x_supino_izq_4'=> $request->x_supino_izq_4,

                               'x_supino_der_1'=> $request->x_supino_der_1,
                               'x_supino_der_2'=> $request->x_supino_der_2,
                               'x_supino_der_3'=> $request->x_supino_der_3,
                               'x_supino_der_4'=> $request->x_supino_der_4,

                               'x_prono_izq_1'=> $request->x_prono_izq_1,
                               'x_prono_izq_2'=> $request->x_prono_izq_2,
                               'x_prono_izq_3'=> $request->x_prono_izq_3,
                               'x_prono_izq_4'=> $request->x_prono_izq_4,

                               'x_prono_der_1'=> $request->x_prono_der_1,
                               'x_prono_der_2'=> $request->x_prono_der_2,
                               'x_prono_der_3'=> $request->x_prono_der_3,
                               'x_prono_der_4'=> $request->x_prono_der_4,

                               'giro_cab' => $request->giro_cab,
                               'pelvis' => $request->pelvis,
                               'rest_sacra' => $request->rest_sacra,
                               'fosa_izq'=> $request->fosa_izq,
                               'fosa_der'=> $request->fosa_der,
                               'angular_omop' => $request->angular_omop,
                               'oblicuo_men' => $request->oblicuo_men,
                               'oblicuo_may' => $request->oblicuo_may,
                               'ecom' => $request->ecom,
                               'com' => $request->com,
                               'recto_post_izq' =>$request->recto_post_izq,
                               'recto_post_der' =>$request->recto_post_der,
                               'inter_trans' =>$request->inter_trans,
                               'espelino_cab' => $request->espelino_cab,
                               'espelino_cue' => $request->espelino_cue,
                               'esc_medio' => $request->esc_medio,
                               'C3' => $request->C3,
                               'C4' => $request->C4,
                               'C5' => $request->C5,
                               'C6' => $request->C6,
                               'C7' => $request->C7,
                               'D1' => $request->D1,
                               'D2' => $request->D2,
                               'D3' => $request->D3,
                               'D4' => $request->D4,
                               'D5' => $request->D5,
                               'D6' => $request->D6,
                               'D7' => $request->D7,
                               'D8' => $request->D8,
                               'D9' => $request->D9,
                               'D10' => $request->D10,
                               'D11' => $request->D11,
                               'D12' => $request->D12,
                               'L1' => $request->L1,
                               'L2' => $request->L2,
                               'L3' => $request->L3,
                               'L4' => $request->L4,
                               'L5' => $request->L5,

                               'il_cost' => $request->il_cost,
                                'dor_l' => $request->dor_l,
                                'ser_p_sup' => $request->ser_p_sup,
                                'ser_p_inf' => $request->ser_p_inf,
                                'SCMa' => $request->SCMa,
                                'SCMe' => $request->SCMe,
                                'Lig_si' => $request->Lig_si,
                                'atlas_patr'=> $request->atlas_patr,
                                'axis_patr' => $request->axis_patr,
                                'inferiores_patr' => $request->inferiores_patr,
                                // 'persona_id' => $id,
                              ]
        );
        $persona = Persona::find($id);
        // Sincronizacion de datos Inserta
        //y actualiza datos de multi select
        $persona->atlas()->sync($request->AtAx);
        $persona->axises()->sync($request->Axis);
        $persona->segmentos()->sync($request->Segmento);
        $persona->delineamientos()->sync($request->Delineamiento);
        $persona->pelvis()->sync($request->Pelvis);



        return redirect()->route('fichaPersonal.show', $request->persona_id);

        // dd($request->AtAx);

 // $atlasSeleccionados =
    }
}
