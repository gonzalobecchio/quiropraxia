<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AgendaPersona extends Pivot
{
    public $timestamps = false;

    protected $hidden  = ['agenda_id','endTime', 'startTime'];
}
