<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    protected $guarded = [];

    protected $primaryKey  = 'persona_id';


    // public function paciente()
    // {
    //     return $this->belongsTo('App\Paciente');
    // }

    public function paciente()
    {
      return $this->belongsTo('App\Paciente');
    }
}
