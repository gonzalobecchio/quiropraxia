<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
  protected $guarded = [];

    public function paciente()
    {
        return $this->belongsTo('App\Paciente');
    }
}
