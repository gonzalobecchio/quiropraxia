<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelvis extends Model
{
    public $timestamps = false;

    public function personas()
    {
      return $this->belongsToMany('App\Persona','pelvis_patron');
    }

    public function controls()
    {
      return $this->belongsToMany('App\Control','pelvis_control');
    }

}
