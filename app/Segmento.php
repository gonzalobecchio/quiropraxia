<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segmento extends Model
{
    public $timestamps = false;

    public function personas()
    {
      return $this->belongsToMany('App\Persona','segmento_patron');
    }

    public function controls()
    {
      return $this->belongsToMany('App\Control','segmento_control');
    }

}
