<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\Relations\Pivot;

class ControlPersona extends Model
{
    protected $table = 'control_persona';
    // public $incrementing = true;
    protected $guarded = [];



    public function user()
    {
      return $this->belongsTo('App\User');
    }

}
