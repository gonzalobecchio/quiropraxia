<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Axis extends Model
{
    public $timestamps = false;

    public function personas()
    {
      return $this->belongsToMany('App\Persona','axis_patron');
    }

    public function controls()
    {
      return $this->belongsToMany('App\Control');
    }

}
