<?php
/**
 *
 */
namespace App\newClass;

class Utilidades
{
  private $datosLocalidades;
  private $datosProvincias;
  private $arrayLocalidades = array();
  private $arrayProvincias = array();

  //Obtenemos todas las provincias desde json
  //Verificar url en caso de de quese caiga la pagina o no tenga acceso a internet
  private $urlLocalidades = 'https://infra.datos.gob.ar/catalog/modernizacion/dataset/7/distribution/7.27/download/localidades-censales.json';

  private $urlProvincias = 'https://infra.datos.gob.ar/catalog/modernizacion/dataset/7/distribution/7.2/download/provincias.json';

 public function __construct()
 {
   //Localidades
    $this->datosLocalidades = json_decode(file_get_contents($this->urlLocalidades),true);
    $this->datosLocalidades = $this->datosLocalidades['localidades-censales'];

    //Provincias
    $this->datosProvincias = json_decode(file_get_contents($this->urlProvincias), true);
    $this->datosProvincias = $this->datosProvincias['provincias'];

 }

 public function cargarProvincias()
 {
   for ($i=0; $i < count($this->datosProvincias); $i++) {
     $id = $this->datosProvincias[$i]['id'];
     $nombre = $this->datosProvincias[$i]['nombre'];
     $this->arrayProvincias[$i] = array('id' => $id,
                                         'nombre' => $nombre);
   }

   // return  response()->json($this->arrayProvincias);
   // return json_encode($this->arrayProvincias);
    return $this->arrayProvincias;
   // print_r($this->datosProvincias['provincias'][]['nombre']);
   // print_r('<br>');
   // print_r($this->datosProvincias['provincias'][2]['id']);
 }


  public function cargarLocalidades()
  {
    for ($i=0; $i < count($this->datosLocalidades); $i++) {

        $id = $this->datosLocalidades[$i]['id']; //id localidad json
        $nombre = $this->datosLocalidades[$i]['nombre']; //localidad
        $provincia_id = $this->datosLocalidades[$i]['provincia']['id'];
        $this->arrayLocalidades[$i] = array('id' => $id,
                                            'nombre' => $nombre,
                                            'provincia_id' => $provincia_id);
    }

    // print_r($this->arrayLocalidades);
    // return json_encode($this->arrayLocalidades);
    return $this->arrayLocalidades;
    // return response()->json($this->arrayLocalidades);

  }

}


 ?>
