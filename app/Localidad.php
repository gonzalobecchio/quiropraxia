<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    public $timestamps = false;
    
    public function provincia()
    {
        return $this->belongsTo('App\Provincia');
    }

    public function persona()
    {
        return $this->hasMany('App\Persona');
    }
}
