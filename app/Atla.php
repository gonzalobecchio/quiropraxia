<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atla extends Model
{
    public $timestamps = false;

    public function personas()
    {
        return $this->belongsToMany('App\Persona', 'atla_patron', 'atla_id', 'persona_id');
    }

    public function controls()
    {
      return $this->belongsToMany('App\Control');
    }



}
