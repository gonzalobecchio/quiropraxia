<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delineamiento extends Model
{
    public $timestamps = false;

    public function personas()
    {
      return $this->belongsToMany('App\Persona','delineamiento_patron');
    }

    public function controls()
    {
      return $this->belongsToMany('App\Control','delineamiento_control');
    }

}
