<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    public function personas()
    {
        return $this->belongsToMany('App\Persona', 'agenda_persona')
      ->using('App\AgendaPersona')
      ->withPivot(['start','end', 'persona_id', 'agenda_id']);
    }
}
