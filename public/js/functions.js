//Pasar a jquery

addEventListener('DOMContentLoaded', inicio, false);

function inicio() {
    document.getElementById('formulario').addEventListener('submit', validar, false);
}


function typeFile(file) {
    //Devuelve verdadero si es un archivo permitido
    const fileTypes = [
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/jpg',
    ];
    return fileTypes.includes(file[0].type);
}



//Validacion de peso de imagen la misma tiene que ser menor a 5MB
function validar(evt) {
    var imagen = document.getElementById('file').files;
    // console.log();
    console.log(imagen.length);
    console.log(typeFile(imagen[0].type));
    evt.preventDefault();
    if (imagen.length > 0) {
      if (typeFile(imagen)) {
          var peso = imagen[0].size / 1048576; //Division para obtener MB
          var peso = Math.round(peso);
          if (peso > 5) { //No ingresa si el peso es mayor a 5MB
              alert(`El peso de ${imagen[0].name} es ${peso} MB, debe ser menos de 5 MB`);
              console.log('Este es el peso de la imagen ' + peso);
              evt.preventDefault();
          }

      } else {
          alert('Solo archivos .jpeg, .png, pjpeg');
          evt.preventDefault();
      }
    }else{
        alert('No hay imagen seleccionada');
        evt.preventDefault();

    }
}
