$(document).ready(init);

function init() {

$('#deleteImg i').click(function (event) {
    if (!confirm('Eliminar imagen?')) {
      event.preventDefault();
    }
  });

$('#deleteVisita i').click(function (event) {
  if (!confirm('Eliminar Visita?')) {
    event.preventDefault();
  }
});

$('#deleteControl i').click(function (event) {
  if (!confirm('Eliminar Control?')) {
    event.preventDefault();
  }
});

$('#deletePaciente i').click(function (event) {
  if (!confirm('Eliminar Paciente?')) {
    event.preventDefault();
  }
});

}
